/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

`ifndef PER_INSTANCE
  `define PER_INSTANCE    1           //For all coverage groups, if ZERO means don't collect per instance
`endif

`ifndef RAPIDIO_AWIDTH
  `define RAPIDIO_AWIDTH	  32          //General address width - will not change
`endif

`ifndef RAPIDIO_DWIDTH
  `define RAPIDIO_DWIDTH	  32          //General data width - will not change
`endif

`ifndef NUM_OF_SLAVES
  `define NUM_OF_SLAVES   1           //Used only in test lib to set config num_slaves
`endif

`ifndef SIMULATION_TIME
  `define SIMULATION_TIME 1000000     //TIMEOUT
`endif

`ifndef ROM_MEM_SIZE
  `define ROM_MEM_SIZE    1024        //Used in target test sequence for ROM
`endif

`ifndef FIFO_DEPTH
  `define FIFO_DEPTH      16          //Used in target test sequence for FIFO
`endif


