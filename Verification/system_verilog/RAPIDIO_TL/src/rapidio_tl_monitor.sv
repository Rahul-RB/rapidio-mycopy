/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_TL_MONITOR_SVH
`define RAPIDIO_TL_MONITOR_SVH

// Callback defined for  Transport Layer agent
virtual class rapidio_tl_monitor_callback extends uvm_callback;
  
  // Constructor for callback
  function new(string name = "rapidio_tl_monitor_callback");
    super.new(name);
  endfunction:new
 
  // Tasks defined in callback
  virtual task tl_error();
  endtask

endclass: rapidio_tl_monitor_callback

// Declaration of monitor class

class rapidio_tl_monitor extends uvm_monitor;

uvm_tlm_fifo #(byte unsigned) dr2mon;

//uvm_analysis_port #(rapidio_tl_sequence_item) rio_tl_ap; 

virtual interface         rapidio_interface  vif;
int mon_un_pack;

 //SB ANALYSIS PORT ADDED HERE 
  uvm_analysis_port#(rapidio_tl_sequence_item)mon2sb_tl_port;
  uvm_analysis_port#(rapidio_tl_sequence_item)mon2sb_tl_dut_port;
  uvm_analysis_port#(rapidio_tl_sequence_item)mon2cov_tl_port;
  uvm_analysis_port#(rapidio_tl_sequence_item)mon_tl_ral_port;

event  packet2receive_tl_monitor;


rapidio_tl_sequence_item resp;
rapidio_tl_sequence_item t2;
rapidio_tl_sequence_item receive_pkt; // queues for storing the transactions
rapidio_tl_agent_config tl_cfg;

uvm_blocking_get_port #( byte unsigned) get_pl_pkt_port;
uvm_blocking_get_port #( int) get_pl_cntrl_port;
uvm_blocking_put_port #( byte unsigned) put_ll_pkt_port;
uvm_blocking_put_port #( int) put_ll_cntrl_port;

bit                          item_available = 0;
//protected rapidio_tl_sequence_item trans_collected;

  // UVM automation macros
  `uvm_component_utils_begin(rapidio_tl_monitor)
    `uvm_field_object(resp,UVM_ALL_ON)
    `uvm_field_object(receive_pkt,UVM_ALL_ON)
    `uvm_field_object(tl_cfg,UVM_ALL_ON)
    `uvm_field_int(item_available,UVM_ALL_ON)
  `uvm_component_utils_end 

  // Register Callback with driver
   `uvm_register_cb(rapidio_tl_monitor,rapidio_tl_monitor_callback)

  // Task & Function declarations  
  extern function new (string name, uvm_component parent);
  extern virtual function void build_phase(uvm_phase phase);
  extern virtual task run_phase(uvm_phase phase);
  extern virtual task drive_reset_values();
  extern virtual task get_resp_pkt(rapidio_tl_sequence_item t2);
  //extern virtual task send_resp_pkt(rapidio_tl_sequence_item resp);

endclass

// FUNC : Component constructor
function rapidio_tl_monitor::new(string name, uvm_component parent); 
  super.new(name,parent);
   resp = new();
   t2 = new();
   dr2mon=new("dr2mon",this);
   get_pl_pkt_port = new("get_pl_pkt_port", this); 
   get_pl_cntrl_port = new("get_pl_cntrl_port", this); 
   put_ll_pkt_port = new("put_ll_pkt_port", this); 
   put_ll_cntrl_port = new("put_ll_cntrl_port", this);
   mon2sb_tl_port=new("mon2sb_tl_port",this);
   mon2sb_tl_dut_port=new("mon2sb_tl_dut_port",this);
   mon2cov_tl_port=new("mon2cov_tl_port",this);
   mon_tl_ral_port=new("mon_tl_ral_port",this);

endfunction: new

function void rapidio_tl_monitor::build_phase(uvm_phase phase);
  super.build_phase(phase);
  if(!uvm_config_db#(virtual rapidio_interface)::get(this, "", "vif", vif))
    `uvm_fatal("RAPIDIO_TL_MON_FATAL",{"VIRTUAL INTERFACE MUST BE SET FOR: ",get_full_name(),".vif"});
  void'(uvm_config_db#(rapidio_tl_agent_config)::get(this,"","rapidio_tl_agent_config",tl_cfg));
endfunction: build_phase


task rapidio_tl_monitor::run_phase(uvm_phase phase);
  super.run_phase(phase);
  drive_reset_values();
   forever begin
    item_available = 1;
    get_resp_pkt(resp);
  end
endtask

// TASK : drive_reset_values - Drive reset values in the RAPIDIO interface
task rapidio_tl_monitor::drive_reset_values();
  `uvm_info("RAPIDIO_TL_DRV_INFO",$sformatf("DRIVING RESET VALUES ON TL LAYER I/F \n"), UVM_LOW);
endtask : drive_reset_values

task rapidio_tl_monitor::get_resp_pkt(rapidio_tl_sequence_item t2);
  int pl_pkt_len;
  int ll_pkt_len;
  byte unsigned pl_bytes [];
  int i=0;
  bit [7:0] unpack_ll_bytes [];
    
    get_pl_cntrl_port.get(pl_pkt_len);
    pl_bytes = new[pl_pkt_len];
    for(i=0; i< pl_pkt_len; i++) begin 
      get_pl_pkt_port.get(pl_bytes[i]);
    end
    t2.pl_size  = pl_pkt_len;
    void'(resp.unpack_bytes(pl_bytes));
    ll_pkt_len = t2.ll_pkt_bytes.size ;
    put_ll_cntrl_port.put(ll_pkt_len);
    //`uvm_info("RAPIDIO_TL_MONITOR_INFO",$sformatf("GOT THE PL SEQ ITEM = %p \n",t2), UVM_LOW);
    foreach (t2.ll_pkt_bytes[i]) 
      begin 
        put_ll_pkt_port.put(t2.ll_pkt_bytes[i]);
      ->packet2receive_tl_monitor;
      end
    item_available    = 1'b0; 
   
  // Sending the transactions to the scorebaord // 
    mon2sb_tl_port.write(t2);
    mon2sb_tl_dut_port.write(t2);
    mon2cov_tl_port.write(t2);
    mon_tl_ral_port.write(t2);
    //`uvm_info("RAPIDIO_TL_MONITOR_INFO",$sformatf("GOT THE PL DATA BYTES \n"), UVM_LOW);
  
endtask



`endif
