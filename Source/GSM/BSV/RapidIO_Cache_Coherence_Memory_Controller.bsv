/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_Cache_Coherence_Memory_Controller;

import RapidIO_Cache_Coherence_Types::*;
import RapidIO_Cache_Coherence_Encode_packet::*;
import DefaultValue::*;





interface Ifc_Memory_Contrl;

method Bool isReady();
method Action stopRequestFor(Physical_Address_Type physical_Address);
method Action _inputs_update_Block(Packet pkt);
method Action _inputs_Packet_Invalidate(Physical_Address_Type physical_Address);
method Action _inputs_Write_Back(Packet pkt);


endinterface

(*synthesize*)
(*always_enabled*)
(*always_ready*)

module mkMemoryContrl(Ifc_Memory_Contrl);

Reg#(Bool) stopRequestForCalled <- mkReg(False);
Reg#(Bool) isRuleRunning <- mkReg(False);
Reg#(Bool) invalidateCalled<- mkReg(False);
Reg#(Bool) write_Back_Called <- mkReg(False);
Reg#(Bool) blockUpdateCalled <- mkReg(False);
//Reg#(Bool) <- mkReg(False);
//Reg#() <- mkReg()
Reg#(Bool) runFirst <- mkReg(True);
Reg#(Physical_Address_Type) rg_coheID <- mkReg(0);
Reg#(Physical_Address_Type) rg_phyAddInv <- mkReg(0);
Reg#(Physical_Address_Type) rg_phyAddWriteMemory <- mkReg(0);
Reg#(Packet) rg_updtBlock <- mkReg(defaultValue);
Reg#(Packet) rg_writeBack<- mkReg(defaultValue);


rule runToRespond(runFirst); // runFirst = true means , this rule will run first.
	
	
	if(stopRequestForCalled == True)begin
		$display("Requets stopped for  %x   ",rg_coheID);
	end	
	
	else if(invalidateCalled == True)begin
	
		$display("%x Invalidated in Main Memroy",rg_phyAddInv);
		
	end
	
	else if(write_Back_Called == True)begin
		$display("%x  Written Back To Main Memory",rg_writeBack.physical_Address);
	end
	
	else if(blockUpdateCalled == True)begin
		$display("%x block is updated",rg_updtBlock.physical_Address);
	end
	
	runFirst <= False;
 	
 	
 	
 
 
endrule

rule runToReset(runFirst);

	stopRequestForCalled <= False;
	invalidateCalled  <= False;
	
	runFirst <= True;
	

endrule




method Bool isReady();
return True;
endmethod

method Action stopRequestFor(Physical_Address_Type physical_Address);

stopRequestForCalled <= True;
rg_coheID <= physical_Address;

endmethod

method Action _inputs_Packet_Invalidate(Physical_Address_Type physical_Address);

rg_phyAddInv <= physical_Address;
invalidateCalled <= True;

endmethod

method Action _inputs_update_Block(Packet pkt);
	rg_updtBlock <= pkt;
	blockUpdateCalled <= True;
endmethod

method Action _inputs_Write_Back(Packet pkt);

	write_Back_Called <= True;
	rg_writeBack <= pkt;
	
endmethod





endmodule





 



endpackage
