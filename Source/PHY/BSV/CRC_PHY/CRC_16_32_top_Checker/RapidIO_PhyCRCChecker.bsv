/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- This Module is a test module to check the entire CRC operation developed, which will be used in the PHY top module 
-- 1.crc32 is checked and stripped off and then data is given to crc16 module.this data only includes crc16 and not crc32.
-- 2.crc16 is checked after stripping crc32 therefore the input is delayed and given to crc16 module.
-- 
--To be done
-- 1.If total length of packet is more than 80 bytes (Two CRC16 codes are appended and both cases have to be checked)
-- 2.If CRC16 or CRC32 code appears in the next cycle after last data cycle,i.e when tx_rem == 101,110 & 111.
-- 3.Stripping off of CRC-16 and giving the data alone to next stage along with appropriate signals like sof,eof,vld,rem etc to the next
-- stage.
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_PhyCRCChecker;

import RapidIO_PhyCRC16Checker::*;
import RapidIO_PhyCRC32Checker::*;

interface Ifc_RapidIO_PhyCRCChecker;
 
//input methods and output methods
	method Action _link_tx_sof_n (Bool value);
	method Action _link_tx_eof_n (Bool value);
	method Action _link_tx_vld_n (Bool value);
	method Action _link_tx_data (Bit#(128) value);
	method Action _link_tx_rem (Bit#(3) value);
	//method Action _link_tx_crf (Bit#(2) value);           //yet to be implemented
 
	method Bit#(16) outputs_CRC16_();
	method Bit#(32) outputs_CRC32_();

endinterface : Ifc_RapidIO_PhyCRCChecker

module mkRapidIO_PhyCRCChecker(Ifc_RapidIO_PhyCRCChecker);

Ifc_RapidIO_PhyCRC16Checker crc16check <- mkRapidIO_PhyCRC16Checker;
Ifc_RapidIO_PhyCRC32Checker crc32check <- mkRapidIO_PhyCRC32Checker;

//Input signals as wires
Wire#(Bool) wr_tx_sof <- mkDWire (True);
Wire#(Bool) wr_tx_eof <- mkDWire (True);
Wire#(Bool) wr_tx_vld <- mkDWire (True);
Wire#(Bit#(128)) wr_tx_data <- mkDWire (0);
Wire#(Bit#(3)) wr_tx_rem <- mkDWire (0);

//Intermediate registers for providing required delays when appending crc16 and also when giving to crc32 module.
Reg#(Bool) rg_in_sof <- mkReg (True);
Reg#(Bool) rg_in_eof <- mkReg (True);
Reg#(Bool) rg_in_vld <- mkReg (True);
Reg#(Bit#(128)) rg_in_data <- mkReg (0);
Reg#(Bit#(3)) rg_in_rem <- mkReg (0);

//output signals as wires
Wire#(Bit#(16)) wr_out_CRC16 <- mkDWire (0);
Wire#(Bit#(32)) wr_out_CRC32 <- mkDWire (0);
Wire#(Bit#(128)) wr_out_data16 <- mkDWire (0);


//input to crc32 checking module
rule r1_crc32;

crc32check._link_rx_sof_n(wr_tx_sof);
crc32check._link_rx_eof_n(wr_tx_eof);
crc32check._link_rx_vld_n(wr_tx_vld);
crc32check._link_rx_data(wr_tx_data);
crc32check._link_rx_rem(wr_tx_rem);

endrule

//delay provided for giving to crc16 module
rule delay;

rg_in_data <= wr_tx_data;
rg_in_sof <= wr_tx_sof;
rg_in_eof <= wr_tx_eof;
rg_in_vld <= wr_tx_vld;
rg_in_rem <= wr_tx_rem;

endrule

//crc32 stripping according to tx_rem 
rule r1_crc32_remov_zero_paddng_crc32;

wr_out_CRC32 <= crc32check.output_CRC32_check_();//crc32 checksum

case(rg_in_rem)

3'b111:wr_out_data16 <= {rg_in_data[127:112],112'b0};
3'b110:wr_out_data16 <= {128'b0};
3'b101:wr_out_data16 <= {128'b0};
3'b100:wr_out_data16 <= {rg_in_data[127:32],32'b0};
3'b011:wr_out_data16 <= {rg_in_data[127:48],48'b0};
3'b010:wr_out_data16 <= {rg_in_data[127:64],64'b0};
3'b001:wr_out_data16 <= {rg_in_data[127:80],80'b0};
3'b000:wr_out_data16 <= {rg_in_data[127:96],96'b0};

endcase

$display("rem value for crc16 check = %b",rg_in_rem);

endrule


//input given to crc16 checking module
rule r2_crc16_check;

$display("data into crc16 check without crc32 = %h",wr_out_data16);

crc16check._link_rx_sof_n(rg_in_sof);
crc16check._link_rx_eof_n(rg_in_eof);
crc16check._link_rx_vld_n(rg_in_vld);
crc16check._link_rx_data((rg_in_eof ? rg_in_data : wr_out_data16));//if eof is false,stripped data is taken,otherwise original data is taken
crc16check._link_rx_rem(rg_in_rem);

endrule


//crc16 checksum
rule r2_crc16_out;

wr_out_CRC16 <= crc16check.output_CRC16_check_();

endrule


//input and output method definitions
method Action _link_tx_sof_n (Bool value);
	wr_tx_sof <= value;
endmethod

method Action _link_tx_eof_n (Bool value);
	wr_tx_eof <= value;
endmethod
 
method Action _link_tx_vld_n (Bool value);
	wr_tx_vld <= value;
endmethod

method Action _link_tx_data (Bit#(128) value);
	wr_tx_data <= value;
endmethod

method Action _link_tx_rem (Bit#(3) value);
	wr_tx_rem <= value;
endmethod
 
method Bit#(16) outputs_CRC16_();//crc16 checksum
	return wr_out_CRC16;
endmethod

method Bit#(32) outputs_CRC32_();//crc32 checksum
	return wr_out_CRC32;
endmethod

endmodule:mkRapidIO_PhyCRCChecker
endpackage:RapidIO_PhyCRCChecker
