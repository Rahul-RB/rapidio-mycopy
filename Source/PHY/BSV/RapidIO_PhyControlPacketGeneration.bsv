/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Main IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. Control symbol generation.
-- 2. Performing CRC 24 for the control symbol and adding with the control symbol packet before transmission
--      
-- To do's
-- 1. Stype 1 control symbols
-- 
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014	, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_PhyControlPacketGeneration;

import RapidIO_PhyCRC24Generation ::*;


interface Ifc_RapidIO_PhyControlPacketGeneration;

// input methods
method Action _inputs_tx_sof_n (Bool value);
method Action _inputs_tx_eof_n (Bool value);
method Action _inputs_tx_vld_n (Bool value);
method Action _inputs_tx_dsc_n (Bool value);

method Action _inputs_rx_ControlSymbolPkt (Bit#(64) cs)

// -- Output Methods
method Bit#(64) outputs_tx_ControlSymbolPkt_ ();
endinterface : Ifc_RapidIO_PhyControlPacketGeneration



function Bit#(64) fn_GenerateControlSymbol_from_CRC (Bit#(4) stype0, Bit#(12) parameter0, Bit#(12) parameter1, Bit#(8) stype1, Bit#(2) alignment, Bit#(24) crc24);
    return {stype0[3:0], parameter0, parameter1, stype1[7:6], alignment, stype1[5:0], crc24, alignment};
endfunction

function Bit#(64) fn_GenerateControlSymbol_to_CRC (Bit#(4) stype0, Bit#(12) parameter0, Bit#(12) parameter1, Bit#(8) stype1, Bit#(2) alignment);
    return {stype0[3:0], parameter0, parameter1, stype1[7:6], alignment, stype1[5:0], 24'h000000, alignment};
endfunction


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIO_PhyControlPacketGeneration (Ifc_RapidIO_PhyControlPacketGeneration);


// Wire#(Bit#(4)) wr_stype0 <- mkDWire(0);
Wire#(Bit#(8)) wr_stype1 <- mkDWire(0);
Wire#(Bit#(2)) wr_alignment <- mkDWire(0);
Wire#(Bit#(24)) wr_tx_crc24 <- mkDWire(0);
Wire#(Bool) wr_tx_sof <- mkDWire(False);
Wire#(Bool) wr_tx_eof <- mkDWire(False);
Wire#(Bool) wr_tx_vld <- mkDWire(False);
Wire#(Bool) wr_tx_dsc <- mkDWire(False);
Wire#(Bit#(64)) wr_rx_ControlSymbolPkt <- mkDWire(0);

Wire#(Bit#(64)) wr_input_to_CRC <- mkDWire(0);
Wire#(Bit#(64)) wr_tx_ControlSymbolPkt <- mkDWire(0);
Wire#(Bool) wr_csfn <- mkDWire(False);


Ifc_RapidIO_PhyCRC24Generation crcforcs <- mkRapidIO_PhyCRC24Generation;

rule rl_SOP(wr_tx_sof == True && wr_tx_vld == True);
//   if (wr_tx_sof == True && wr_tx_vld == True) begin
	//wr_stype1[5:0] <= 6'b100010; //Allignment and CRC yet to be added, as of now it is taken as zero
	//wr_stype1[7:6] <= 2'b10;
	wr_stype1 <= 8'b10100010; //ackID yet to be implemented
   	wr_csfn <= True;
//    end
//   else
//	wr_csfn <= False;
endrule

rule rl_EOP(wr_tx_eof == True && wr_tx_vld == True);
//   if (wr_tx_eof == True && wr_tx_vld == True) begin
	//wr_stype1[5:0] <= 6'b100010; //Allignment and CRC yet to be added, as of now it is taken as zero
	//wr_stype1[7:6] <= 2'b10;
	wr_stype1 <= 8'b10101111;
   	wr_csfn <= True;
//end
//else
//	wr_csfn <= False;

endrule

rule rl_restart_from_retry (wr_rx_ControlSymbolPkt[63:60] == 4b'0001);  //wr_stype0[3:0] <= 4'b0001

// restart from retry should also intimate the buffer module for sending a higher priority packet,
// also  logic has to be written in phy lane strriping for sending control symbol (RFR) withput packet which results --> (RFR)(SOP), p1 p2 p3,... (EOP)

	wr_stype1 <= 8'b00011000;
   	wr_csfn <= True;
		
endrule

rule rl_link_request

endrule

rule rl_input_to_CRC_fn;
   if (wr_csfn == True) begin
	wr_input_to_CRC <= fn_GenerateControlSymbol_to_CRC(4'h0, 12'h000, 12'h000, wr_stype1, wr_alignment);
   end
endrule


rule rl_input_to_CRC_module;
  if (wr_csfn == True) begin
	crcforcs._inputs_data_in(wr_input_to_CRC[63:26]);			
	crcforcs._inputs_tx_vld(False);
     $display("\n input to crc module =  %b ",wr_input_to_CRC);
   end
endrule

rule rl_CRCgeneration;
   if (wr_csfn == True) begin
	wr_tx_crc24 <= crcforcs.outputs_CRC24_ ();
   end
endrule

rule rl_ControlSymbolPkt;
   if (wr_csfn == True) begin
	wr_tx_ControlSymbolPkt <= fn_GenerateControlSymbol_from_CRC(4'h0, 12'h000, 12'h000, wr_stype1, wr_alignment, wr_tx_crc24);
$display("\n crc 24 =  %b ",wr_tx_crc24); 
   end

endrule

/*
rule rl_stype_encoding;

// Assigning the encoding values as per the specification

case (wr_stype1_encoding) matches
	'd1 : begin	//Stomp - cancel a partially transmitted packet
	wr_stype1[5:0] <= 6'b001000;
	wr_stype1[8:6] <= 2'b00;

	wr_tx_vld <= False; // Cancel the packet

	end

	'd2 : begin	// end of padding unpadded
	wr_stype1[5:0] <= 6'b010000;
	wr_stype1[8:6] <= 2'b00;
	end

// Will be added once the entire packet is formulated

	'd3 : begin	// end of padding padded
	wr_stype1[5:0] <= 6'b010001;
	wr_stype1[8:6] <= 2'b00;
	end
// Will be added once the entire packet is formulated

	'd4 : begin	// restart from retry-to cancel a packet
	wr_stype1[5:0] <= 6'b011000;
	wr_stype1[8:6] <= 2'b00;
	
	wr_tx_vld <= False;
	

	end

	'd5 : begin	// link request/reset port-to cancel a packet
	wr_stype1[5:0] <= 6'b100010;
	wr_stype1[8:6] <= 2'b00;
	end

	'd6 : begin	// link request/reset device-to cancel a packet
	wr_stype1[5:0] <= 6'b100011;
	wr_stype1[8:6] <= 2'b00;
	end

	'd7 : begin	// link request/port status-to cancel a packet
	wr_stype1[5:0] <= 6'b100100;
	wr_stype1[8:6] <= 2'b00;
	end

	'd8 : begin	// multicast event
	wr_stype1[5:0] <= 6'b101000;
	wr_stype1[8:6] <= 2'b00;
	end

	'd9 : begin	// loop timing request
	wr_stype1[5:0] <= 6'b101011;
	wr_stype1[8:6] <= 2'b00;
	end

	'd10 : begin	// start of padding unpadded
	wr_stype1[5:0] <= ackID[5:0];
	wr_stype1[8:6] <= 2'b10;
	end

	'd11 : begin	// start of padding padded
	wr_stype1[5:0] <= ackID[5:0];
	wr_stype1[8:6] <= 2'b11;
	end

$display ("No control symbol NOP \n ");
	wr_stype1[5:0] <= 6'b111000;	// NOP
	wr_stype1[8:6] <= 2'b00;
	end  
endcase

case (wr_stype0_encoding) matches
	'd1 : begin	// Packet Accepted Control symbol
	wr_stype0[3:0] <= 4'b0000;
	end

	'd2 : begin	// Packet Retry Control symbol
	wr_stype0[3:0] <= 4'b0001;
	end

	'd3 : begin	// Packet-Not-Accepted Control symbol
	wr_stype0[3:0] <= 4'b0010;
	end

	'd4 : begin	// Timestamp Control symbol
	wr_stype0[3:0] <= 4'b0011;
	end

	'd5 : begin	// Status Control symbol
	wr_stype0[3:0] <= 4'b0100;
	end

	'd6 : begin	// VC status Control symbol
	wr_stype0[3:0] <= 4'b0101;
	end

	'd7 : begin	// Link Response Control symbol
	wr_stype0[3:0] <= 4'b0110;
	end

	'd8 : begin	// Loop Response Control symbol
	wr_stype0[3:0] <= 4'b1011;
	end

	default : begin
$display ("No control symbol\n ");
	wr_stype0[3:0] <= 4'b1111;
	wr_stype1[5:0] <= 6'b000000;
	wr_stype1[8:6] <= 2'b00;
	end  
endcase

endrule
*/



// Methods Defintions
// -- Input Methods 
method Action _inputs_tx_sof_n (Bool value);
    wr_tx_sof <= !(value);
 endmethod

method Action _inputs_tx_eof_n (Bool value);
    wr_tx_eof <= !(value);
 endmethod 

method Action _inputs_tx_vld_n (Bool value);
    wr_tx_vld <= !(value); 
 endmethod

method Action _inputs_tx_dsc_n (Bool value);
    wr_tx_dsc <= !(value); 
 endmethod


//method Action _inputs_rx_ControlSymbolPkt (Bit#(64) cs);
//    wr_rx_ControlSymbolPkt <= cs;
// endmethod

//method Action _inputs_tx_CRC24 (Bit#(24) CRC);
  //  wr_tx_CRC24 <= CRC;
 //endmethod

// -- Output Methods
 method Bit#(64) outputs_tx_ControlSymbolPkt_();
    return wr_tx_ControlSymbolPkt;
 endmethod

endmodule : mkRapidIO_PhyControlPacketGeneration

endpackage : RapidIO_PhyControlPacketGeneration

