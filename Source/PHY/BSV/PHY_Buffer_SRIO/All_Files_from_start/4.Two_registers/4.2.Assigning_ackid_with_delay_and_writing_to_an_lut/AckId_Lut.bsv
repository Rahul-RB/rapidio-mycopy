package AckId_Lut;

//import RegFile::*;


interface Ifc_AckId_Lut;
	method Action _ackid(Bit#(6) value);
	method Action _rd_ptr_in(Bit#(4) value);
	method Action _identify(Bit#(2) value);
	method Bit#(4) rd_ptr_out_();
endinterface:Ifc_AckId_Lut

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkAckId_Lut(Ifc_AckId_Lut);


//RegFile#(Bit#(2),Bit#(2))  l1 <- mkRegFile(0,3);//ackid-address index ,data-rdptr 

Wire#(Bit#(6)) ack_id <- mkDWire(0);
Wire#(Bit#(4)) rd_ptr_in <- mkDWire(0);
Wire#(Bit#(2)) id <- mkDWire(0);
Wire#(Bit#(4)) rd_ptr_out <- mkDWire(0);



rule r1(id == 2'b01);
l1.upd(ack_id,rd_ptr_in);
$display("%b,%b",ack_id,rd_ptr_in);
endrule

rule r2(id == 2'b10);
rd_ptr_out <= l1.sub(ack_id);

endrule

/*rule r2(id == 1);
case(ack_id)
  2'b00:y[3] <= rd_ptr_in;
  2'b01:y[2] <= rd_ptr_in;
  2'b00:y[1] <= rd_ptr_in;
  2'b00:y[0] <= rd_ptr_in;
endcase
endrule


rule r1;

//if(id == 1)
//begin
case(ack_id)
  2'b00:rd_ptr_out <= y[3];
  2'b01:rd_ptr_out <= y[2];
  2'b00:rd_ptr_out <= y[1];
  2'b00:rd_ptr_out <= y[0];
endcase
endrule*/

//else










method Action _ackid(Bit#(6) value);
       ack_id <= value;
endmethod

method Action _rd_ptr_in(Bit#(4) value);
       rd_ptr_in <= value;
endmethod

method Action _identify(Bit#(2) value);
       id <= value;
endmethod

method Bit#(4) rd_ptr_out_();
       return rd_ptr_out;
endmethod


endmodule:mkAckId_Lut
endpackage:AckId_Lut
