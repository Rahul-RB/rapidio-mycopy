package Tb_RapidIOPhy_Buffer_Prio_All_Cycles;

import RapidIOPhy_Buffer_Prio_all ::*;

(*synthesize*)
//(*always_enabled*)
//(*always_ready*)


module mkTb_RapidIOPhy_Buffer_Prio_All_Cycles(Empty);


Ifc_RapidIOPhy_Buffer_Prio_all bufr <- mkRapidIOPhy_Buffer_Prio_all;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 55 )
		$finish (0);
endrule


rule rl1(reg_ref_clk == 0);
$display("Register default value is %h",bufr.buf_out_00_());
endrule

rule rl2(reg_ref_clk == 1);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h002C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 00
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
endrule

rule rl3(reg_ref_clk == 2);
$display("Regstr value is %h",bufr.buf_out_00_());
endrule

rule rl4(reg_ref_clk == 3);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
endrule

rule rl5(reg_ref_clk == 4);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00775A8D9C8EFC0ACEA0F52AD3E66EF0);//prio 01
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
//$display("Regstr value is %h",bufr.buf_out_01_());
endrule

rule rl6(reg_ref_clk == 5);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
//$display("Regstr value is %h",bufr.buf_out_01_());
endrule

rule rl7(reg_ref_clk == 6);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00BC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 10
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_10_());
endrule

rule rl7_1(reg_ref_clk == 7);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00BC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_10_());
endrule

rule rl7_2(reg_ref_clk == 8);
bufr._tx_sof_n(False);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00BC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 10
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_10_());
endrule

rule rl8(reg_ref_clk == 9);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h003C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 00
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_00_());
endrule

rule rl9(reg_ref_clk == 10);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h003C11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_00_());
endrule

rule rl10(reg_ref_clk == 11);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h002AD3E66EFC11375A8D9C8EFC0ACEA2);//prio 00
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
endrule

rule rl10_1(reg_ref_clk == 12);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h002AD3E66EFC11375A8D9C8EFC0ACEA2);
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
endrule

rule rl11(reg_ref_clk == 13);
$display("Regstr value is %h",bufr.buf_out_00_());
endrule

rule rl12(reg_ref_clk == 14);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 11
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
endrule

rule rl12_1(reg_ref_clk == 15);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b1111);
bufr._tx_crf(False);
endrule



rule rl13_a(reg_ref_clk == 16);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h003C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 00
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_10_());
endrule


rule rl13_b(reg_ref_clk == 17);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h003C11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_10_());
endrule

rule rl13(reg_ref_clk == 18);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00BC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 10
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_10_());
endrule



rule rl14(reg_ref_clk == 19);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_11_());
endrule

rule rl14_2(reg_ref_clk == 20);
bufr._tx_sof_n(False);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 11
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_11_());
endrule

rule rl14_1(reg_ref_clk == 21);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 11
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_11_());
endrule

rule rl14_3(reg_ref_clk == 22);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_11_());
endrule

rule rl15(reg_ref_clk == 23);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h007C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 01
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_01_());
endrule

rule rl15_1(reg_ref_clk == 24);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h007C11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_01_());
endrule

rule rl15_2(reg_ref_clk == 25);
bufr._tx_sof_n(False);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h007C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 01
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
//$display("Regstr value is %h",bufr.buf_out_01_());
endrule

rule rl16(reg_ref_clk == 26);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 11
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl16_1(reg_ref_clk == 27);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00FC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl17(reg_ref_clk == 28);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h007C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 01
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl17_1(reg_ref_clk == 29);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h007C11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl18(reg_ref_clk == 30);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00BC11376A8D9C8EFC0ACEA0F62AD3E6);//prio 10
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl18_1(reg_ref_clk == 31);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h00BC11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl19(reg_ref_clk == 32);
bufr._tx_sof_n(False);
bufr._tx_eof_n(True);
bufr._tx_vld_n(False);
bufr._tx_data(128'h002C11376A8D9C8EFC0ACEA0F62AD3E6);//prio 00
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule

rule rl19_1(reg_ref_clk == 33);
bufr._tx_sof_n(True);
bufr._tx_eof_n(False);
bufr._tx_vld_n(False);
bufr._tx_data(128'h002C11376A8D9C8EFC0ACEA0F62AD3E6);
bufr._tx_rem(4'b0111);
bufr._tx_crf(True);
endrule


rule rl_disp_out(reg_ref_clk == 7);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_a(reg_ref_clk == 8);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_b(reg_ref_clk == 9);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_c(reg_ref_clk == 10);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_d(reg_ref_clk == 11);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_e(reg_ref_clk == 12);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_f(reg_ref_clk == 13);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_g(reg_ref_clk == 14);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_h(reg_ref_clk == 15);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_i(reg_ref_clk == 16);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_j(reg_ref_clk == 17);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_k(reg_ref_clk == 18);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_l(reg_ref_clk == 19);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_m(reg_ref_clk == 20);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule



rule rl_disp_out_n(reg_ref_clk == 21);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_o(reg_ref_clk == 22);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_p(reg_ref_clk == 23);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_q(reg_ref_clk == 24);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_r(reg_ref_clk == 25);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_s(reg_ref_clk == 26);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_t(reg_ref_clk == 27);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl14_a(reg_ref_clk == 46);
bufr._tx_stop_txmsn(1'b1);
bufr._ack_id_retrans(6'b000010);
endrule

rule rl_disp_out_u(reg_ref_clk == 28);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_v(reg_ref_clk == 29);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_w(reg_ref_clk == 30);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl17_a(reg_ref_clk == 50);
bufr._tx_retransmsn(1'b1);
endrule

rule rl_disp_out_x(reg_ref_clk == 31);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl_disp_out_y(reg_ref_clk == 32);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl_disp_out_z(reg_ref_clk == 33);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl_disp_out_1(reg_ref_clk == 34);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl_disp_out_2(reg_ref_clk == 35);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_3(reg_ref_clk == 36);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_4(reg_ref_clk == 37);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_5(reg_ref_clk == 38);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_6(reg_ref_clk == 39);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_7(reg_ref_clk == 40);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_8(reg_ref_clk == 41);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_9(reg_ref_clk == 42);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_10(reg_ref_clk == 43);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl_disp_out_11(reg_ref_clk == 44);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_12(reg_ref_clk == 45);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_13(reg_ref_clk == 46);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl_disp_out_14(reg_ref_clk == 47);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_15(reg_ref_clk == 48);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule

rule rl_disp_out_16(reg_ref_clk == 49);
$display("sof is %b",bufr.lnk_tsof_n_());
$display("vld is %b",bufr.lnk_tvld_n_());
$display("eof is %b",bufr.lnk_teof_n_());
$display("DATA is %h",bufr.lnk_td_());
$display("Rem is %b",bufr.lnk_trem_());
$display("CRF is %b",bufr.lnk_tcrf_());
endrule


rule rl20(reg_ref_clk == 53);
bufr._tx_deq(1'b1);
bufr._ack_id_deq(6'b000000);
endrule



endmodule:mkTb_RapidIOPhy_Buffer_Prio_All_Cycles
endpackage:Tb_RapidIOPhy_Buffer_Prio_All_Cycles
