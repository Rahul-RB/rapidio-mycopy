package RapidIOPhy_Buffer11;

import RapidIOPhy_Buffer_read_enable ::*;
import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer2 ::*;
//import AckId_Lut::*;
import AckId_generator::*;


interface Ifc_RapidIOPhy_Buffer11;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   //method Action _tx_rdy_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf(Bit#(2) value);
   //method Action _tx_read_en(Bit#(1) value);
   //method Action _tx_rg12(Bit#(4) value);
   method Action _tx_deq(Bit#(1) value);
   //method Action _ack_id(Bit#(2) value);
   //method Action _tx_ack(Bit#(4) value);
   //method Action _tx_read(Bit#(1) value);
   method Action _tx_retransmsn(Bit#(1) value);        //enabling retxmsn
   method Action _tx_stop_txmsn(Bit#(1) value);        //to stop txmtng when either PNA or PR control symbol comes
   method Action _ack_id(Bit#(6) value);               //dequeing the acknowledged ackid
   
   method RegBuf buf_out_();
   method Bool lnk_tvld_n_();
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bit#(4) lnk_trem_();
   method Bit#(2) lnk_tcrf_();

   method Bit#(4) out_wrt_ptr_();                     //to the read write pointer module to determine when to start reading 
   method Bit#(4) out_rd_ptr_();   

endinterface:Ifc_RapidIOPhy_Buffer11



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer11(Ifc_RapidIOPhy_Buffer11);

//set of 11 registers that will make up the lowest priority area in the buffer

Ifc_RapidIOPhy_Buffer2 reg1 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg2 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg3 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg4 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg5 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg6 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg7 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg8 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg9 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg10 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg11 <- mkRapidIOPhy_Buffer2;

Ifc_AckId_generator ack1 <- mkAckId_generator;
Ifc_RapidIOPhy_Buffer_read_enable rd_enbl <- mkRapidIOPhy_Buffer_read_enable;

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
//Wire#(Bool) tx_rdy_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bit#(2)) wr_tx_crf <- mkDWire(0);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4))  wr_lnk_trem <- mkDWire(0);
Wire#(Bit#(2))  wr_lnk_tcrf <- mkDWire(0);

Reg#(Bit#(4)) rg_wrt_ptr <- mkReg(4'b0001);      //pointer indicating to which register write is happening.
Reg#(Bit#(4)) rg_read_ptr <- mkReg(4'b0000);     //pointer indicating from which register read is happening.
//Wire#(Bit#(4)) wr_read_intr <- mkDWire(0);
Reg#(Bit#(1)) rg_read <- mkReg(0);
Wire#(Bit#(1)) wr_read <- mkDWire(0);
Wire#(Bit#(1)) wr_deq <- mkDWire(0);
Wire#(RegBuf) buf_out <- mkDWire(0);
Wire#(Bit#(6))  wr_tx_ack <- mkDWire(0);
Wire#(Bit#(6)) wr_ackid <- mkDWire(0);       //6 bit ackid coming through 6 bit bus
Wire#(Bit#(1))  wr_retrans <- mkDWire(0);
Wire#(Bit#(1))  wr_stop_txmsn <- mkDWire(0);
Wire#(Bit#(4)) wr_wrt_ptr <- mkDWire(0);//writing
Wire#(Bit#(4)) wr_read_ptr <- mkDWire(0);//reading

rule r1_wr_ptr;                                         //moving to next register whenever eof comes

	wr_wrt_ptr <= rg_wrt_ptr;
	$display("wrt_ptr =%b",rg_wrt_ptr);

	if(wr_tx_eof_n == False)
		begin
		if(rg_wrt_ptr == 4'b1011) 
			rg_wrt_ptr <= 4'b0001;
		else
			rg_wrt_ptr <= rg_wrt_ptr + 1;
		end

endrule

rule r1_ptr_out;

	rd_enbl._in_wrt_ptr(rg_wrt_ptr);
	rd_enbl._in_rd_ptr(rg_read_ptr);
	wr_read_ptr <= rg_read_ptr;
	rd_enbl._tx_retransmsn(wr_retrans);
	rd_enbl._tx_stop_txmsn(wr_stop_txmsn);
endrule


rule r1_rd_en;
	rg_read <= rd_enbl.tx_read_en_();                //read enabling
endrule

rule read_en_copy;
	wr_read <= rg_read;
endrule



rule retrans(wr_retrans == 1);
	//$display("read_ptr_in_top_module =%b",wr_read_ptr);
	if(wr_read_ptr == 4'b0001)
		reg1._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b0010)
		reg2._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b0011)
		reg3._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b0100)
		reg4._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b0101)
		reg5._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b0110)
		reg6._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b0111)
		reg7._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b1000)
		reg8._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b1001)
		reg9._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b1010)
		reg10._tx_retransmsn(wr_retrans);
	else if(wr_read_ptr == 4'b1011)
		reg11._tx_retransmsn(wr_retrans);
endrule


rule read_initial((rg_wrt_ptr >= 4 && rg_read_ptr == 0) || (wr_retrans == 1));          //condition under which read is enabled
	rg_read_ptr <= rd_enbl.read_ptr_init_();             //read pointer is given the value of first register
endrule

/*rule read_ptr_retrans(wr_retrans == 1);
	rg_read_ptr <= rd_enbl.read_ptr_init_();
endrule*/

rule r1_rd_ptr_incr(wr_read == 1);
	//$display("read_en_in_buffer = %b",rg_read);

	if(reg1.read_eof_() == 1 || reg2.read_eof_() == 1 || reg3.read_eof_() == 1 || reg4.read_eof_() == 1 || reg5.read_eof_() == 1 ||      			reg6.read_eof_() == 1 || reg7.read_eof_() == 1 || reg8.read_eof_() == 1 || reg9.read_eof_() == 1 || reg10.read_eof_() == 1 			|| reg11.read_eof_() == 1)
			begin
			$display("entering.................");
			ack1._ack_en(False);                     //ackid generation enabling
			if(rg_read_ptr == 4'b1011) 
				begin
//a1._ack_en(False);
				rg_read_ptr <= 4'b0001;
				end
			else
				begin
//a1._ack_en(False);
				rg_read_ptr <= rg_read_ptr + 1;//without considering retransmission
				end
			end
endrule

//rule ack;
//a1._ack_en(False);
//endrule


rule r1_vld;

//$display("wr_wrt_ptr = %b",wr_wrt_ptr);
	if(wr_wrt_ptr == 4'b0001)
		reg1._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b0010)
		reg2._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b0011)
		reg3._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b0100)
		reg4._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b0101)
		reg5._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b0110)
		reg6._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b0111)
		reg7._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b1000)
		reg8._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b1001)
		reg9._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b1010)
		reg10._tx_vld_n(wr_tx_vld_n);
	else if(wr_wrt_ptr == 4'b1011)
		reg11._tx_vld_n(wr_tx_vld_n);

endrule

rule r1_sof;

	if(wr_wrt_ptr == 4'b0001)
		reg1._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b0010)
		reg2._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b0011)
		reg3._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b0100)
		reg4._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b0101)
		reg5._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b0110)
		reg6._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b0111)
		reg7._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b1000)
		reg8._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b1001)
		reg9._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b1010)
		reg10._tx_sof_n(wr_tx_sof_n);
	else if(wr_wrt_ptr == 4'b1011)
		reg11._tx_sof_n(wr_tx_sof_n);

endrule

rule r1_eof;

	if(wr_wrt_ptr == 4'b0001)
		reg1._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b0010)
		reg2._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b0011)
		reg3._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b0100)
		reg4._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b0101)
		reg5._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b0110)
		reg6._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b0111)
		reg7._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b1000)
		reg8._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b1001)
		reg9._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b1010)
		reg10._tx_eof_n(wr_tx_eof_n);
	else if(wr_wrt_ptr == 4'b1011)
		reg11._tx_eof_n(wr_tx_eof_n);

endrule



rule r1_data;

	if(wr_wrt_ptr == 4'b0001)
		reg1._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b0010)
		reg2._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b0011)
		reg3._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b0100)
		reg4._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b0101)
		reg5._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b0110)
		reg6._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b0111)
		reg7._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b1000)
		reg8._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b1001)
		reg9._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b1010)
		reg10._tx_data(wr_tx_data);
	else if(wr_wrt_ptr == 4'b1011)
		reg11._tx_data(wr_tx_data);

endrule

rule r1_rem;

	if(wr_wrt_ptr == 4'b0001)
		reg1._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b0010)
		reg2._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b0011)
		reg3._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b0100)
		reg4._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b0101)
		reg5._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b0110)
		reg6._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b0111)
		reg7._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b1000)
		reg8._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b1001)
		reg9._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b1010)
		reg10._tx_rem(wr_tx_rem);
	else if(wr_wrt_ptr == 4'b1011)
		reg11._tx_rem(wr_tx_rem);

endrule

rule r1_crf;

	if(wr_wrt_ptr == 4'b0001)
		reg1._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b0010)
		reg2._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b0011)
		reg3._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b0100)
		reg4._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b0101)
		reg5._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b0110)
		reg6._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b0111)
		reg7._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b1000)
		reg8._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b1001)
		reg9._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b1010)
		reg10._tx_crf_n(wr_tx_crf);
	else if(wr_wrt_ptr == 4'b1011)
		reg11._tx_crf_n(wr_tx_crf);

endrule

//(wr_read == 1)
rule ackid_return;
	wr_tx_ack <= ack1.ack_id_();                        //ackid generated
	$display("read_ptr =%b",rg_read_ptr);

endrule
//(rg_read == 1)
rule disp;
	$display("ackid original =%b",wr_tx_ack);
$display("rg read in top == %b",rg_read);
endrule
/*
rule disp1;
	$display("value of read ptr in clock 24 25 26 etc =%b",rd_enbl.read_ptr_init_());
endrule*/
//(wr_read == 1)
rule r1_ack;
//$display("wire read pointer = %b",wr_read_ptr);
	if(rg_read_ptr == 4'b0001)
//begin

		reg1._tx_ack(wr_tx_ack);
//end
	else if(rg_read_ptr == 4'b0010)
begin
//$display("ackid in rule= %b",wr_tx_ack);
		reg2._tx_ack(wr_tx_ack);
end
	else if(rg_read_ptr == 4'b0011)
		reg3._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0100)
		reg4._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0101)
		reg5._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0110)
		reg6._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0111)
		reg7._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b1000)
		reg8._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b1001)
		reg9._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b1010)
		reg10._tx_ack(wr_tx_ack);
//$display(""wr_tx_ack == %b",wr_tx_ack);
	else if(rg_read_ptr == 4'b1011)
		reg11._tx_ack(wr_tx_ack);

endrule

rule r1_read(wr_read == 1);
//$display("wire read pointer in next rule= %b",wr_read_ptr);

	if(wr_read_ptr == 4'b0001)

		reg1._tx_read(wr_read);
	else if(wr_read_ptr == 4'b0010)
//begin
//$display("rg read check in rule == %b",,wr_read);
		reg2._tx_read(wr_read);
//end
	else if(wr_read_ptr == 4'b0011)

		reg3._tx_read(wr_read);

	else if(wr_read_ptr == 4'b0100)
		reg4._tx_read(wr_read);
	else if(wr_read_ptr == 4'b0101)
		reg5._tx_read(wr_read);
	else if(wr_read_ptr == 4'b0110)
		reg6._tx_read(wr_read);
	else if(wr_read_ptr == 4'b0111)
		reg7._tx_read(wr_read);
	else if(wr_read_ptr == 4'b1000)
		reg8._tx_read(wr_read);
	else if(wr_read_ptr == 4'b1001)
		reg9._tx_read(wr_read);
	else if(wr_read_ptr == 4'b1010)
		reg10._tx_read(wr_read);
	else if(wr_read_ptr == 4'b1011)
		reg11._tx_read(wr_read);

endrule

/*rule r1_read1(rg_read == 1);
if(wr_wrt_ptr == 2'b10)
begin
r1._rd_ptr_in(wr_wrt_ptr);

//r1._tx_read(rg_read);
end
else if(wr_wrt_ptr == 2'b11)
begin
r2._rd_ptr_in(wr_wrt_ptr);

//r2._tx_read(rg_read);
end
endrule*/

rule r1_deq(wr_deq == 1);//when rg_buf[ackid]==lnk_last_ack,deq = 1;position of a particular ackid to be found out from lookup table.
//a2._identify(2'b10);
//a2._ackid(ackid);
//$display("fire");
	if(wr_ackid == 6'b000000)     
		reg1._tx_deq(wr_deq);
	else if(wr_ackid ==6'b000001)
		reg2._tx_deq(wr_deq);
	else if(wr_ackid == 6'b000010)
		reg3._tx_deq(wr_deq);
	else if(wr_ackid == 6'b000011)
		reg4._tx_deq(wr_deq);
	else if(wr_ackid == 6'b000100)
		reg5._tx_deq(wr_deq);
	else if(wr_ackid == 6'b000101)
		reg6._tx_deq(wr_deq);
	else if(wr_ackid == 6'b000110)
		reg7._tx_deq(wr_deq);
	else if(wr_ackid == 6'b000111)
		reg8._tx_deq(wr_deq);
	else if(wr_ackid == 6'b001000)
		reg9._tx_deq(wr_deq);
	else if(wr_ackid == 6'b001001)
		reg10._tx_deq(wr_deq);
	else if(wr_ackid == 6'b001010)
		reg11._tx_deq(wr_deq);

endrule

rule r1_lnk_vld(wr_read == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_tvld_n <= reg1.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_tvld_n <= reg2.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_tvld_n <= reg3.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_tvld_n <= reg4.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_tvld_n <= reg5.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_tvld_n <= reg6.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_tvld_n <= reg7.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b1000)
		wr_lnk_tvld_n <= reg8.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b1001)
		wr_lnk_tvld_n <= reg9.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b1010)
		wr_lnk_tvld_n <= reg10.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b1011)
		wr_lnk_tvld_n <= reg11.lnk_tvld_n_();

endrule

rule r1_lnk_sof(wr_read == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_tsof_n <= reg1.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_tsof_n <= reg2.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_tsof_n <= reg3.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_tsof_n <= reg4.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_tsof_n <= reg5.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_tsof_n <= reg6.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_tsof_n <= reg7.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b1000)
		wr_lnk_tsof_n <= reg8.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b1001)
		wr_lnk_tsof_n <= reg9.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b1010)
		wr_lnk_tsof_n <= reg10.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b1011)
		wr_lnk_tsof_n <= reg11.lnk_tsof_n_();

endrule

rule r1_lnk_eof(wr_read == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_teof_n <= reg1.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_teof_n <= reg2.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_teof_n <= reg3.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_teof_n <= reg4.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_teof_n <= reg5.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_teof_n <= reg6.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_teof_n <= reg7.lnk_teof_n_();
	else if(wr_read_ptr == 4'b1000)
		wr_lnk_teof_n <= reg8.lnk_teof_n_();
	else if(wr_read_ptr == 4'b1001)
		wr_lnk_teof_n <= reg9.lnk_teof_n_();
	else if(wr_read_ptr == 4'b1010)
		wr_lnk_teof_n <= reg10.lnk_teof_n_();
	else if(wr_read_ptr == 4'b1011)
		wr_lnk_teof_n <= reg11.lnk_teof_n_();

endrule

rule r1_lnk_data(wr_read == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_td <= reg1.lnk_td_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_td <= reg2.lnk_td_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_td <= reg3.lnk_td_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_td <= reg4.lnk_td_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_td <= reg5.lnk_td_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_td <= reg6.lnk_td_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_td <= reg7.lnk_td_();
	else if(wr_read_ptr == 4'b1000)
		wr_lnk_td <= reg8.lnk_td_();
	else if(wr_read_ptr == 4'b1001)
		wr_lnk_td <= reg9.lnk_td_();
	else if(wr_read_ptr == 4'b1010)
		wr_lnk_td <= reg10.lnk_td_();
	else if(wr_read_ptr == 4'b1011)
		wr_lnk_td <= reg11.lnk_td_();

endrule

rule r1_lnk_rem(wr_read == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_trem <= reg1.lnk_trem_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_trem <= reg2.lnk_trem_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_trem <= reg3.lnk_trem_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_trem <= reg4.lnk_trem_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_trem <= reg5.lnk_trem_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_trem <= reg6.lnk_trem_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_trem <= reg7.lnk_trem_();
	else if(wr_read_ptr == 4'b1000)
		wr_lnk_trem <= reg8.lnk_trem_();
	else if(wr_read_ptr == 4'b1001)
		wr_lnk_trem <= reg9.lnk_trem_();
	else if(wr_read_ptr == 4'b1010)
		wr_lnk_trem <= reg10.lnk_trem_();
	else if(wr_read_ptr == 4'b1011)
		wr_lnk_trem <= reg11.lnk_trem_();

endrule

rule r1_lnk_crf(wr_read == 1);

	if(wr_read_ptr == 4'b0001)
			wr_lnk_tcrf <= reg1.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0010)
			wr_lnk_tcrf <= reg2.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0011)
			wr_lnk_tcrf <= reg3.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0100)
			wr_lnk_tcrf <= reg4.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0101)
			wr_lnk_tcrf <= reg5.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0110)
			wr_lnk_tcrf <= reg6.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0111)
			wr_lnk_tcrf <= reg7.lnk_tcrf_();
	else if(wr_read_ptr == 4'b1000)
			wr_lnk_tcrf <= reg8.lnk_tcrf_();
	else if(wr_read_ptr == 4'b1001)
			wr_lnk_tcrf <= reg9.lnk_tcrf_();
	else if(wr_read_ptr == 4'b1010)
			wr_lnk_tcrf <= reg10.lnk_tcrf_();
	else if(wr_read_ptr == 4'b1011)
			wr_lnk_tcrf <= reg11.lnk_tcrf_();

endrule

rule r1_lnk_out;

	if(wr_wrt_ptr == 4'b0001)
//begin
		buf_out <= reg1.buf_out_();
//$display("Data stored in register is %h",buf_out);
//end
	else if(wr_wrt_ptr == 4'b0010)
		buf_out <= reg2.buf_out_();
	else if(wr_wrt_ptr == 4'b0011)
		buf_out <= reg3.buf_out_();
	else if(wr_wrt_ptr == 4'b0100)
		buf_out <= reg4.buf_out_();
	else if(wr_wrt_ptr == 4'b0101)
		buf_out <= reg5.buf_out_();
	else if(wr_wrt_ptr == 4'b0110)
		buf_out <= reg6.buf_out_();
	else if(wr_wrt_ptr == 4'b0111)
		buf_out <= reg7.buf_out_();
	else if(wr_wrt_ptr == 4'b1000)
		buf_out <= reg8.buf_out_();
	else if(wr_wrt_ptr == 4'b1001)
		buf_out <= reg9.buf_out_();
	else if(wr_wrt_ptr == 4'b1010)
		buf_out <= reg10.buf_out_();
	else if(wr_wrt_ptr == 4'b1011)
		buf_out <= reg11.buf_out_();

endrule















method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _tx_crf(Bit#(2) value);
     wr_tx_crf <= value;
endmethod

method Action _tx_retransmsn(Bit#(1) value);
     wr_retrans <= value;
endmethod

method Action _tx_stop_txmsn(Bit#(1) value);
     wr_stop_txmsn <= value;
endmethod

//method Action _tx_read_en(Bit#(1) value);
	//rg_read <= value;
//endmethod
/*method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod*/

//method Action _tx_rg12(Bit#(4) value);
     //wr_wrt_ptr <= value;
//endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

/*method Action _tx_ack(Bit#(4) value);
		wr_tx_ack <= value;
endmethod*/


method Action _ack_id(Bit#(6) value);
     wr_ackid <= value;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method RegBuf buf_out_();
     return buf_out;
endmethod

method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return  wr_lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return  wr_lnk_tcrf;
endmethod

method Bit#(4) out_wrt_ptr_();
	return rg_wrt_ptr;
endmethod

method Bit#(4) out_rd_ptr_();
	return rg_read_ptr;
endmethod 

endmodule:mkRapidIOPhy_Buffer11
endpackage:RapidIOPhy_Buffer11
