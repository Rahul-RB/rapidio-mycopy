package Tb_RapidIOPhy_Buffer11;

import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer11 ::*;
import RapidIOPhy_Buffer2 ::*;
import AckId_generator::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)

module mkTb_RapidIOPhy_Buffer11(Empty);

Ifc_RapidIOPhy_Buffer11 bfr6 <- mkRapidIOPhy_Buffer11;
//Ifc_RapidIOPhy_Buffer5 bfr5 <- mkRapidIOPhy_Buffer5;
//Ifc_AckId_generator a1 <- mkAckId_generator;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);
//Wire#(Bit#(2)) rg_x <- mkDWire (0);
//Reg#(Bit#(2)) rg_q <- mkReg (0);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 65 )
		$finish (0);
endrule

rule r10(reg_ref_clk == 0);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_1(reg_ref_clk == 1);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hF52AD3E66EFC11375A8D9C8EFC0ACEA2);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule



rule r1_3_16(reg_ref_clk == 2);
//bfr6._tx_vld_n(False);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3(reg_ref_clk == 3);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b1111);
bfr6._tx_crf(2'b11);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule



//rule r102(reg_ref_clk == 13);
//bfr6._tx_rg12(4'b0010);
//endrule

rule r2_1(reg_ref_clk == 4);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EF0);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2(reg_ref_clk == 5);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r15(reg_ref_clk == 6);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_3(reg_ref_clk == 7);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule





rule r2_1xc(reg_ref_clk == 8);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h5A8D9C8EFC0ACEA0F52AD3E66EF0113F);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2s(reg_ref_clk == 9);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r2_2w(reg_ref_clk == 10);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2e(reg_ref_clk == 11);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r15c(reg_ref_clk == 12);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_3d(reg_ref_clk == 13);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b1011);
bfr6._tx_crf(2'b01);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_x(reg_ref_clk == 14);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EF0);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_m(reg_ref_clk == 15);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r56(reg_ref_clk == 16);
$display("Regstr value is %h",bfr6.buf_out_());
endrule


rule a1_3_4i(reg_ref_clk == 16);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r2_3z(reg_ref_clk == 17);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule b1_3_3i(reg_ref_clk == 17);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_1x(reg_ref_clk == 18);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EF0);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule c1_3_3i(reg_ref_clk == 18);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_2c(reg_ref_clk == 19);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule


rule d1_3_3i(reg_ref_clk == 19);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r15cx(reg_ref_clk == 20);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule e1_3_3i(reg_ref_clk == 20);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_3a(reg_ref_clk == 21);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule



rule f1_3_3i(reg_ref_clk == 21);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_1s(reg_ref_clk == 22);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EF0);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule g1_3_3i(reg_ref_clk == 22);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_2d(reg_ref_clk == 23);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule


rule h1_3_3i(reg_ref_clk == 23);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule
//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r15d(reg_ref_clk == 24);
$display("Regstr value is %h",bfr6.buf_out_());
endrule




rule i1_3_3i(reg_ref_clk == 24);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_3f(reg_ref_clk == 25);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule


rule j1_3_3i(reg_ref_clk == 25);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r15cf(reg_ref_clk == 25);
bfr6._tx_stop_txmsn(1'b1);
endrule


//rule r15c(reg_ref_clk == 27);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule


//-------------------------oooooooooooooooooooooooooooooooooo---------------------------------
//ooooooooooooooooooooooooooooo--------------------
//rule r101wv(reg_ref_clk == 28);
//bfr6._tx_rg12(4'b0001);
//endrule

/*rule r1_3_1f1(reg_ref_clk == 29);
//a1._ack_en(False);
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_25r(reg_ref_clk == 30);
//a1._ack_en(True);
//bfr6._tx_ack(a1.ack_id_());
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_256t(reg_ref_clk == 31);
//a1._ack_en(False);
//bfr6._tx_ack(a1.ack_id_());
//bfr6._tx_read(1'b1);
//rg_q <= a1.ack_id_();
//$display("rg_q value is %b",rg_q);
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_256rg(reg_ref_clk == 32);
//a1._ack_en(False);
//bfr6._tx_ack(a1.ack_id_());

//$display("rg_q value is %b",rg_q);
$display("ack value is %b",a1.ack_id_());
endrule



rule r1_3_3v(reg_ref_clk == 33);
//bfr6._tx_read(1'b1);
bfr6._tx_ack(a1.ack_id_());
endrule*/

rule r2_3v(reg_ref_clk == 26);

bfr6._tx_sof_n(False);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
//bfr6._tx_stop_txmsn(1'b1);

//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_3i(reg_ref_clk == 26);
//bfr6._tx_read(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_3a(reg_ref_clk == 27);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4(reg_ref_clk == 28);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4q(reg_ref_clk == 29);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4w(reg_ref_clk == 30);
//bfr6._tx_retransmsn(1'b1);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r15m(reg_ref_clk == 30);
bfr6._tx_retransmsn(1'b1);
endrule


rule r1_3_4e(reg_ref_clk == 31);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4r(reg_ref_clk == 32);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4t(reg_ref_clk == 33);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4y(reg_ref_clk == 34);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4u(reg_ref_clk == 35);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4ak(reg_ref_clk == 36);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4sl(reg_ref_clk == 37);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4i(reg_ref_clk == 38);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule



rule r1_3_4o(reg_ref_clk == 39);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4p(reg_ref_clk == 40);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4a(reg_ref_clk == 41);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4s(reg_ref_clk == 42);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4d(reg_ref_clk == 43);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4f(reg_ref_clk == 44);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4g(reg_ref_clk == 45);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4h(reg_ref_clk == 46);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule
rule r1_3_4j(reg_ref_clk == 47);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4k(reg_ref_clk == 48);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4l(reg_ref_clk == 49);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4z(reg_ref_clk == 50);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule



rule r1_3_4c(reg_ref_clk == 51);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4v(reg_ref_clk == 52);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4b(reg_ref_clk == 53);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4n(reg_ref_clk == 54);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4m(reg_ref_clk == 55);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4aq(reg_ref_clk == 56);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4ws(reg_ref_clk == 57);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4cd(reg_ref_clk == 58);
//bfr6._tx_read(1'b0);
//$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_8(reg_ref_clk == 59);
bfr6._tx_deq(1'b1);
bfr6._ack_id(6'b0);

$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_8_1(reg_ref_clk == 60);
bfr6._tx_deq(1'b0);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_4_8(reg_ref_clk == 61);
//bfr6._tx_rg12(4'b0001);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r11(reg_ref_clk == 62);
$display("Regstr value is %h",bfr6.buf_out_());
endrule



endmodule:mkTb_RapidIOPhy_Buffer11
endpackage:Tb_RapidIOPhy_Buffer11
