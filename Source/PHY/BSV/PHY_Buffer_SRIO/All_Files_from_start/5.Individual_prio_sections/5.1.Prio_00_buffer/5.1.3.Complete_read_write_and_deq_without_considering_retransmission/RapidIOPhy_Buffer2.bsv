//Top module for the set of 11 registers in the buffer which holds the lowest priority packets



package RapidIOPhy_Buffer2;

import RapidIO_DTypes ::*;


interface Ifc_RapidIOPhy_Buffer2;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf_n(Bit#(2) value);
   method Action _tx_deq(Bit#(1) value);             //to enable dequeing.
   method Action _tx_read(Bit#(1) value);            //to enable reading.
   method Action _tx_ack(Bit#(6) value);
   
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bool lnk_tvld_n_();
   method Bit#(4) lnk_trem_();
   method Bit#(2) lnk_tcrf_();
   method Bit#(1) read_eof_();                         //to indicate end of packet while reading.
   method RegBuf buf_out_();



endinterface:Ifc_RapidIOPhy_Buffer2



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer2(Ifc_RapidIOPhy_Buffer2);

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bit#(2)) wr_tx_crf <- mkDWire(0);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4)) wr_lnk_trem <- mkDWire(0);
Wire#(Bit#(2)) wr_lnk_tcrf <- mkDWire(0);
Wire#(Bit#(1)) wr_eof <- mkDWire(0);
Wire#(Bit#(6)) wr_tx_ack <- mkDWire(0);                //6 bit ack id;actually ackid is 12 bits long but only lower 6 bits are given to packet


Reg#(RegBuf) rg_buf <- mkReg(0);                     //buffer register to which data is written.total register size = 2312 bits
Reg#(Bit#(1)) rg_read <- mkReg(0);                   //reg for enabling reading ie making read and write mutually exclusive 
Reg#(Bit#(5)) rg_cnt_rd <- mkReg(0);                 //read pointer in a single register
Reg#(Bit#(5)) rg_cnt_wr <- mkReg(0);                 //write pointer indicating the position of each cycle of data in the register.
Wire#(Bit#(1)) wr_deq <- mkDWire(0);                 //reg for deciding about dequeue.            




rule r1_write(wr_tx_vld_n == False || wr_deq == 1);                            ///writing into register
//Bit#(12) a ;
  
 	if(wr_deq == 1)
		rg_buf[2311:0] <= 2312'b0;
	else if(wr_tx_sof_n == False && wr_tx_eof_n == False)
		begin            
		rg_buf[2311:0] <= {1'b1,wr_tx_crf,wr_tx_rem,1'b1,2176'b0,wr_tx_data};  //only one cycle(128 bits) of data; [2311:2304] stores eof crf rem and sof in 8 bits
		rg_cnt_wr <= rg_cnt_wr +1;              //write pointer incremented once if only single cycle of data
		rg_cnt_rd <= 0; 
		end

	else if(wr_tx_sof_n == False)
		begin            
		rg_buf[127:0] <= wr_tx_data;                                      //first cycle always goes to position [127:0]
		rg_cnt_wr <= rg_cnt_wr + 4;             //pointer incremented by four in the first cycle in case of multiple cycles of data.

		end

	else if(wr_tx_eof_n == False)
		begin
		let lv_rg_intrmediate = rg_buf[2175:128];
		rg_buf[(2311):(128)] <= {1'b1,wr_tx_crf,wr_tx_rem,1'b1,wr_tx_data,lv_rg_intrmediate};  
   //last cycle always goes to position [2303:2176]; [2311:2304] stores eof crf rem and sof in 8 bits
		rg_cnt_rd <= 0; 
		end 

	else 
		begin
//Bit#(12) b = a+127;
//Bit#(12) lv_pos = rg_pos;
//numeric type lv_pos = rg_pos;
//Integer lv_pos = pack(rg_pos);
//let lv_posn = valueOf(lv_pos1);
		let lv_rg_shift_in_data = rg_buf[2175:128];
		lv_rg_shift_in_data = lv_rg_shift_in_data >> 128;                                 //data is first written to msb of local register and then shifted to right whenever next 128 bits come. 
		lv_rg_shift_in_data[2047:1920]= lv_rg_shift_in_data[2047:1920] | wr_tx_data;   //data is first written to msb of local register(local register is of 2048 bits excluding initial 8 bits and storage area of first cycle and last cycle of data.)
		rg_buf[2175:128]<= lv_rg_shift_in_data;
//rg_buf[(lv_pos+127):lv_pos] <= wr_tx_data;
//rg_pos <= rg_pos + 128;
		rg_cnt_wr <= rg_cnt_wr +1;
		end

      

endrule






rule r1_read(rg_read == 1);                 //reading enabled by rg_read

                                            //pointer = 1 implies single cycle of data
	if(rg_cnt_wr == 1) 
//&& rg_cnt_rd == 0)
		begin
		if(rg_cnt_rd == 0)
			begin
			rg_buf[5:0] <= wr_tx_ack;        //ackid assignment
			//$display("ackid in basic =%b",wr_tx_ack);
			$display("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			rg_cnt_rd <= rg_cnt_rd + 1;
			end
		else if(rg_cnt_rd == 1)
			begin
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= False;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[127:0];
			wr_lnk_trem <= rg_buf[2308:2305];
			wr_lnk_tcrf <= rg_buf[2310:2309];
			wr_eof <= 1'b1;                 //to facilitate read pointer incrementing
			rg_cnt_rd <= 0;
			rg_cnt_wr <= 0;
			end
		end


	else if(rg_cnt_wr != 0 && rg_cnt_wr != 1 && rg_cnt_wr != 2)

		begin
		if(rg_cnt_rd == 0)
			begin
			rg_buf[5:0] <= wr_tx_ack;         //ackid assignment
			//$display("ackid in basic =%b",wr_tx_ack);
			$display("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			rg_cnt_wr <= rg_cnt_wr-1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			end



		else if(rg_cnt_rd == 1)
			begin
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[127:0];
			rg_cnt_wr <= rg_cnt_wr-1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			//wr_lnk_trem <= rg_buf[2308:2305];
			//wr_lnk_tcrf <= rg_buf[2310:2309];
			//wr_eof <= 1'b1;
			end
		else if(rg_cnt_wr == 18)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[255:128];
			rg_cnt_rd <= rg_cnt_rd + 1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 17)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[383:256];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 16)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[511:384];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 15)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[639:512];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 14)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[767:640];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 13)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[895:768];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 12)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1023:896];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 11)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1151:1024];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 10)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1279:1152];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 9)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1407:1280];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 8)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1535:1408] ;
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 7)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1663:1536];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 6)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1791:1664];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 5)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[1919:1792];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 4)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[2047:1920];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_wr == 3)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[2175:2048];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end



		end


	else if(rg_cnt_wr == 2 && rg_cnt_rd != 0)
		begin
		wr_lnk_teof_n <= False;
		wr_lnk_tsof_n <= True;
		wr_lnk_tvld_n <= False;
		wr_lnk_td <= rg_buf[2303:2176];
		wr_eof <= 1'b1;                                //facilitating read pointer incrementing
		wr_lnk_trem <= rg_buf[2308:2305];
		wr_lnk_tcrf <= rg_buf[2310:2309];
		rg_cnt_rd <= 0;



		end
		
endrule
/*
rule r1_read(rg_buf[2245] == 1'b1);

//Bit#(8) a = rg_cnt;
if(rg_buf[2240] == 1'b1)
begin
wr_lnk_tsof_n <= False;
wr_lnk_td <= rg_buf[127:0];
rg_buf[2240] <= 1'b0;
rg_cnt <= rg_cnt - 1;
a <= 128;
end

else
begin
wr_lnk_td <= rg_buf[(a+127):a];
a <= a+128;
rg_cnt <= rg_cnt - 1;
end

if(rg_cnt == 1)
begin
wr_lnk_teof_n <= False;
rg_buf[2245] <= 1'b0;
wr_lnk_trem <= rg_buf[2244:2241];
end

endrule
*/
method Action _tx_ack(Bit#(6) value);
	wr_tx_ack <= value;
//$display("ackid in method = %b",value);
endmethod

method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_crf_n(Bit#(2) value);
     wr_tx_crf <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod



method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return wr_lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return wr_lnk_tcrf;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method Bit#(1) read_eof_();
	return wr_eof;
endmethod

method RegBuf buf_out_();
      return rg_buf;
endmethod

endmodule:mkRapidIOPhy_Buffer2
endpackage:RapidIOPhy_Buffer2
