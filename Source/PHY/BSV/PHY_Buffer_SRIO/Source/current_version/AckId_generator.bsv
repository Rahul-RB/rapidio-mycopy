/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO AckID Generator Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2. AckID for the packet and control symbol is generated.
-- 
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package AckId_generator;

interface Ifc_AckId_generator;
	method Action _ack_en(Bool value);
	method Action _ack_in(Bit#(6) value);
	method Action _retrans(Bool value);

	method Bit#(6) ack_id_out_();
endinterface:Ifc_AckId_generator

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)

//In case retransmission is asked for,at the same time as when ackid is to be incremented,retransmission must be given preference.
(* descending_urgency = "retrns,ak_id_incr" *)

module mkAckId_generator(Ifc_AckId_generator);

//initial value of ackid on reset - zero
Reg#(Bit#(12)) rg_new_ackid <- mkReg(12'b0); 
                                
Wire#(Bool) wr_enbl <- mkDWire(True);
Wire#(Bit#(6)) wr_ackid_in <- mkDWire(0);
Wire#(Bool) wr_retrans <- mkDWire(False);


rule ak_id_incr(wr_enbl == False);
//maximum value of ackid is 12 bits 
	if(rg_new_ackid == 12'hFFF)             
		rg_new_ackid <= 12'b0;// returning to initial value
	else
//ackid sequential in nature;spec part 6 - section 2.4.
		rg_new_ackid <= rg_new_ackid + 1;  
endrule


//***********************************TO BE USED DURING RETRANSMISSION*****************************************************
rule retrns(wr_retrans == True);
	let lv_rg_new_ackid = rg_new_ackid;
//ackid msbs from control symbol to be given instead of lv_rg if required
	rg_new_ackid <= {lv_rg_new_ackid[11:6],wr_ackid_in};
endrule
//*************************************************************************************************************************


//Input Methods
method Action _ack_in(Bit#(6) value);
	wr_ackid_in <=value;
endmethod

method Action _ack_en(Bool value);
	wr_enbl <= value;
endmethod

method Action _retrans(Bool value);
	wr_retrans <= value;
endmethod

//Output Method
method Bit#(6) ack_id_out_();
      return rg_new_ackid[5:0];
endmethod

endmodule:mkAckId_generator
endpackage:AckId_generator
