/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Maintenance Request Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. It receives the Maintenance Request Signals as a Packet from the RxPktFtypeAnalyse module.
-- 2. It depacketize the Maintenance Request packet and generate Maintenance Request output signals
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


//
// Generated by Bluespec Compiler, version 2013.05.beta2 (build 31258, 2013-05-21)
//
// On Mon Apr 21 12:26:59 IST 2014
//
// Method conflict info:
// Method: _inputs_MaintenanceReqIfcPkt
// Conflict-free: outputs_TxReady_From_MReq_, _MaintenanceReqIfc__mreq_rdy_n
// Sequenced before (restricted): _MaintenanceReqIfc_mreq_sof_n_,
// 			       _MaintenanceReqIfc_mreq_eof_n_,
// 			       _MaintenanceReqIfc_mreq_vld_n_,
// 			       _MaintenanceReqIfc_mreq_tt_,
// 			       _MaintenanceReqIfc_mreq_data_,
// 			       _MaintenanceReqIfc_mreq_crf_,
// 			       _MaintenanceReqIfc_mreq_prio_,
// 			       _MaintenanceReqIfc_mreq_ftype_,
// 			       _MaintenanceReqIfc_mreq_ttype_,
// 			       _MaintenanceReqIfc_mreq_dest_id_,
// 			       _MaintenanceReqIfc_mreq_source_id_,
// 			       _MaintenanceReqIfc_mreq_tid_,
// 			       _MaintenanceReqIfc_mreq_offset_,
// 			       _MaintenanceReqIfc_mreq_byte_en_,
// 			       _MaintenanceReqIfc_mreq_byte_count_,
// 			       _MaintenanceReqIfc_mreq_local_
// Conflicts: _inputs_MaintenanceReqIfcPkt
//
// Method: outputs_TxReady_From_MReq_
// Conflict-free: _inputs_MaintenanceReqIfcPkt,
// 	       outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _MaintenanceReqIfc__mreq_rdy_n
//
// Method: _MaintenanceReqIfc_mreq_sof_n_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_eof_n_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_vld_n_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc__mreq_rdy_n
// Conflict-free: _inputs_MaintenanceReqIfcPkt,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced before (restricted): outputs_TxReady_From_MReq_
// Conflicts: _MaintenanceReqIfc__mreq_rdy_n
//
// Method: _MaintenanceReqIfc_mreq_tt_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_data_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_crf_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_prio_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_ftype_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_ttype_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_dest_id_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_source_id_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_tid_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_offset_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_byte_en_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_byte_count_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
// Method: _MaintenanceReqIfc_mreq_local_
// Conflict-free: outputs_TxReady_From_MReq_,
// 	       _MaintenanceReqIfc_mreq_sof_n_,
// 	       _MaintenanceReqIfc_mreq_eof_n_,
// 	       _MaintenanceReqIfc_mreq_vld_n_,
// 	       _MaintenanceReqIfc__mreq_rdy_n,
// 	       _MaintenanceReqIfc_mreq_tt_,
// 	       _MaintenanceReqIfc_mreq_data_,
// 	       _MaintenanceReqIfc_mreq_crf_,
// 	       _MaintenanceReqIfc_mreq_prio_,
// 	       _MaintenanceReqIfc_mreq_ftype_,
// 	       _MaintenanceReqIfc_mreq_ttype_,
// 	       _MaintenanceReqIfc_mreq_dest_id_,
// 	       _MaintenanceReqIfc_mreq_source_id_,
// 	       _MaintenanceReqIfc_mreq_tid_,
// 	       _MaintenanceReqIfc_mreq_offset_,
// 	       _MaintenanceReqIfc_mreq_byte_en_,
// 	       _MaintenanceReqIfc_mreq_byte_count_,
// 	       _MaintenanceReqIfc_mreq_local_
// Sequenced after (restricted): _inputs_MaintenanceReqIfcPkt
//
//
// Ports:
// Name                         I/O  size props
// outputs_TxReady_From_MReq_     O     1
// _MaintenanceReqIfc_mreq_sof_n_  O     1
// _MaintenanceReqIfc_mreq_eof_n_  O     1
// _MaintenanceReqIfc_mreq_vld_n_  O     1
// _MaintenanceReqIfc_mreq_tt_    O     2
// _MaintenanceReqIfc_mreq_data_  O    64
// _MaintenanceReqIfc_mreq_crf_   O     1
// _MaintenanceReqIfc_mreq_prio_  O     2
// _MaintenanceReqIfc_mreq_ftype_  O     4
// _MaintenanceReqIfc_mreq_ttype_  O     4
// _MaintenanceReqIfc_mreq_dest_id_  O    32
// _MaintenanceReqIfc_mreq_source_id_  O    32
// _MaintenanceReqIfc_mreq_tid_   O     8
// _MaintenanceReqIfc_mreq_offset_  O    21
// _MaintenanceReqIfc_mreq_byte_en_  O     8
// _MaintenanceReqIfc_mreq_byte_count_  O     9
// _MaintenanceReqIfc_mreq_local_  O     1
// CLK                            I     1 unused
// RST_N                          I     1 unused
// _inputs_MaintenanceReqIfcPkt_value  I   191
// _MaintenanceReqIfc__mreq_rdy_n_value  I     1
// EN__inputs_MaintenanceReqIfcPkt  I     1
// EN__MaintenanceReqIfc__mreq_rdy_n  I     1
//
// Combinational paths from inputs to outputs:
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_sof_n_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_eof_n_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_vld_n_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_tt_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_data_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_crf_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_prio_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_ftype_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_ttype_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_dest_id_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_source_id_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_tid_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_offset_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_byte_en_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_byte_count_
//   (_inputs_MaintenanceReqIfcPkt_value,
//    EN__inputs_MaintenanceReqIfcPkt) -> _MaintenanceReqIfc_mreq_local_
//   (_MaintenanceReqIfc__mreq_rdy_n_value,
//    EN__MaintenanceReqIfc__mreq_rdy_n) -> outputs_TxReady_From_MReq_
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module mkRapidIO_MaintenanceReqIFC(CLK,
				   RST_N,

				   _inputs_MaintenanceReqIfcPkt_value,
				   EN__inputs_MaintenanceReqIfcPkt,

				   outputs_TxReady_From_MReq_,

				   _MaintenanceReqIfc_mreq_sof_n_,

				   _MaintenanceReqIfc_mreq_eof_n_,

				   _MaintenanceReqIfc_mreq_vld_n_,

				   _MaintenanceReqIfc__mreq_rdy_n_value,
				   EN__MaintenanceReqIfc__mreq_rdy_n,

				   _MaintenanceReqIfc_mreq_tt_,

				   _MaintenanceReqIfc_mreq_data_,

				   _MaintenanceReqIfc_mreq_crf_,

				   _MaintenanceReqIfc_mreq_prio_,

				   _MaintenanceReqIfc_mreq_ftype_,

				   _MaintenanceReqIfc_mreq_ttype_,

				   _MaintenanceReqIfc_mreq_dest_id_,

				   _MaintenanceReqIfc_mreq_source_id_,

				   _MaintenanceReqIfc_mreq_tid_,

				   _MaintenanceReqIfc_mreq_offset_,

				   _MaintenanceReqIfc_mreq_byte_en_,

				   _MaintenanceReqIfc_mreq_byte_count_,

				   _MaintenanceReqIfc_mreq_local_);
  input  CLK;
  input  RST_N;

  // action method _inputs_MaintenanceReqIfcPkt
  input  [190 : 0] _inputs_MaintenanceReqIfcPkt_value;
  input  EN__inputs_MaintenanceReqIfcPkt;

  // value method outputs_TxReady_From_MReq_
  output outputs_TxReady_From_MReq_;

  // value method _MaintenanceReqIfc_mreq_sof_n_
  output _MaintenanceReqIfc_mreq_sof_n_;

  // value method _MaintenanceReqIfc_mreq_eof_n_
  output _MaintenanceReqIfc_mreq_eof_n_;

  // value method _MaintenanceReqIfc_mreq_vld_n_
  output _MaintenanceReqIfc_mreq_vld_n_;

  // action method _MaintenanceReqIfc__mreq_rdy_n
  input  _MaintenanceReqIfc__mreq_rdy_n_value;
  input  EN__MaintenanceReqIfc__mreq_rdy_n;

  // value method _MaintenanceReqIfc_mreq_tt_
  output [1 : 0] _MaintenanceReqIfc_mreq_tt_;

  // value method _MaintenanceReqIfc_mreq_data_
  output [63 : 0] _MaintenanceReqIfc_mreq_data_;

  // value method _MaintenanceReqIfc_mreq_crf_
  output _MaintenanceReqIfc_mreq_crf_;

  // value method _MaintenanceReqIfc_mreq_prio_
  output [1 : 0] _MaintenanceReqIfc_mreq_prio_;

  // value method _MaintenanceReqIfc_mreq_ftype_
  output [3 : 0] _MaintenanceReqIfc_mreq_ftype_;

  // value method _MaintenanceReqIfc_mreq_ttype_
  output [3 : 0] _MaintenanceReqIfc_mreq_ttype_;

  // value method _MaintenanceReqIfc_mreq_dest_id_
  output [31 : 0] _MaintenanceReqIfc_mreq_dest_id_;

  // value method _MaintenanceReqIfc_mreq_source_id_
  output [31 : 0] _MaintenanceReqIfc_mreq_source_id_;

  // value method _MaintenanceReqIfc_mreq_tid_
  output [7 : 0] _MaintenanceReqIfc_mreq_tid_;

  // value method _MaintenanceReqIfc_mreq_offset_
  output [20 : 0] _MaintenanceReqIfc_mreq_offset_;

  // value method _MaintenanceReqIfc_mreq_byte_en_
  output [7 : 0] _MaintenanceReqIfc_mreq_byte_en_;

  // value method _MaintenanceReqIfc_mreq_byte_count_
  output [8 : 0] _MaintenanceReqIfc_mreq_byte_count_;

  // value method _MaintenanceReqIfc_mreq_local_
  output _MaintenanceReqIfc_mreq_local_;

  // signals for module outputs
  wire [63 : 0] _MaintenanceReqIfc_mreq_data_;
  wire [31 : 0] _MaintenanceReqIfc_mreq_dest_id_,
		_MaintenanceReqIfc_mreq_source_id_;
  wire [20 : 0] _MaintenanceReqIfc_mreq_offset_;
  wire [8 : 0] _MaintenanceReqIfc_mreq_byte_count_;
  wire [7 : 0] _MaintenanceReqIfc_mreq_byte_en_, _MaintenanceReqIfc_mreq_tid_;
  wire [3 : 0] _MaintenanceReqIfc_mreq_ftype_, _MaintenanceReqIfc_mreq_ttype_;
  wire [1 : 0] _MaintenanceReqIfc_mreq_prio_, _MaintenanceReqIfc_mreq_tt_;
  wire _MaintenanceReqIfc_mreq_crf_,
       _MaintenanceReqIfc_mreq_eof_n_,
       _MaintenanceReqIfc_mreq_local_,
       _MaintenanceReqIfc_mreq_sof_n_,
       _MaintenanceReqIfc_mreq_vld_n_,
       outputs_TxReady_From_MReq_;

  // rule scheduling signals
  wire CAN_FIRE__MaintenanceReqIfc__mreq_rdy_n,
       CAN_FIRE__inputs_MaintenanceReqIfcPkt,
       WILL_FIRE__MaintenanceReqIfc__mreq_rdy_n,
       WILL_FIRE__inputs_MaintenanceReqIfcPkt;

  // remaining internal signals
  reg [31 : 0] lv_MreqDestID__h502, lv_MreqSrcID__h590;
  wire [63 : 0] x__read_mreqdata_mreq_data__h453;
  wire [31 : 0] x__read_mreqdata_mreq_dest_id__h458,
		x__read_mreqdata_mreq_source_id__h459;
  wire [20 : 0] x__read_mreqdata_mreq_offset__h461;
  wire [8 : 0] x__read_mreqdata_mreq_byte_count__h463;
  wire [7 : 0] x__read_mreqdata_mreq_byte_en__h462,
	       x__read_mreqdata_mreq_tid__h460;
  wire [3 : 0] x__read_mreqdata_mreq_ftype__h456,
	       x__read_mreqdata_mreq_ttype__h457;
  wire [1 : 0] x__read_mreqdata_mreq_tt__h452;

  // action method _inputs_MaintenanceReqIfcPkt
  assign CAN_FIRE__inputs_MaintenanceReqIfcPkt = 1'd1 ;
  assign WILL_FIRE__inputs_MaintenanceReqIfcPkt =
	     EN__inputs_MaintenanceReqIfcPkt ;

  // value method outputs_TxReady_From_MReq_
  assign outputs_TxReady_From_MReq_ =
	     EN__MaintenanceReqIfc__mreq_rdy_n &&
	     _MaintenanceReqIfc__mreq_rdy_n_value ;

  // value method _MaintenanceReqIfc_mreq_sof_n_
  assign _MaintenanceReqIfc_mreq_sof_n_ =
	     !EN__inputs_MaintenanceReqIfcPkt ||
	     !_inputs_MaintenanceReqIfcPkt_value[190] ;

  // value method _MaintenanceReqIfc_mreq_eof_n_
  assign _MaintenanceReqIfc_mreq_eof_n_ =
	     !EN__inputs_MaintenanceReqIfcPkt ||
	     !_inputs_MaintenanceReqIfcPkt_value[189] ;

  // value method _MaintenanceReqIfc_mreq_vld_n_
  assign _MaintenanceReqIfc_mreq_vld_n_ =
	     !EN__inputs_MaintenanceReqIfcPkt ||
	     !_inputs_MaintenanceReqIfcPkt_value[188] ;

  // action method _MaintenanceReqIfc__mreq_rdy_n
  assign CAN_FIRE__MaintenanceReqIfc__mreq_rdy_n = 1'd1 ;
  assign WILL_FIRE__MaintenanceReqIfc__mreq_rdy_n =
	     EN__MaintenanceReqIfc__mreq_rdy_n ;

  // value method _MaintenanceReqIfc_mreq_tt_
  assign _MaintenanceReqIfc_mreq_tt_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_tt__h452 :
	       2'd0 ;

  // value method _MaintenanceReqIfc_mreq_data_
  assign _MaintenanceReqIfc_mreq_data_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[188]) ?
	       x__read_mreqdata_mreq_data__h453 :
	       64'd0 ;

  // value method _MaintenanceReqIfc_mreq_crf_
  assign _MaintenanceReqIfc_mreq_crf_ =
	     EN__inputs_MaintenanceReqIfcPkt &&
	     _inputs_MaintenanceReqIfcPkt_value[190] &&
	     _inputs_MaintenanceReqIfcPkt_value[121] ;

  // value method _MaintenanceReqIfc_mreq_prio_
  assign _MaintenanceReqIfc_mreq_prio_ =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[120:119] :
	       2'd0 ;

  // value method _MaintenanceReqIfc_mreq_ftype_
  assign _MaintenanceReqIfc_mreq_ftype_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_ftype__h456 :
	       4'd0 ;

  // value method _MaintenanceReqIfc_mreq_ttype_
  assign _MaintenanceReqIfc_mreq_ttype_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_ttype__h457 :
	       4'd0 ;

  // value method _MaintenanceReqIfc_mreq_dest_id_
  assign _MaintenanceReqIfc_mreq_dest_id_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       lv_MreqDestID__h502 :
	       32'd0 ;

  // value method _MaintenanceReqIfc_mreq_source_id_
  assign _MaintenanceReqIfc_mreq_source_id_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       lv_MreqSrcID__h590 :
	       32'd0 ;

  // value method _MaintenanceReqIfc_mreq_tid_
  assign _MaintenanceReqIfc_mreq_tid_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_tid__h460 :
	       8'd0 ;

  // value method _MaintenanceReqIfc_mreq_offset_
  assign _MaintenanceReqIfc_mreq_offset_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_offset__h461 :
	       21'd0 ;

  // value method _MaintenanceReqIfc_mreq_byte_en_
  assign _MaintenanceReqIfc_mreq_byte_en_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_byte_en__h462 :
	       8'd0 ;

  // value method _MaintenanceReqIfc_mreq_byte_count_
  assign _MaintenanceReqIfc_mreq_byte_count_ =
	     (EN__inputs_MaintenanceReqIfcPkt &&
	      _inputs_MaintenanceReqIfcPkt_value[190]) ?
	       x__read_mreqdata_mreq_byte_count__h463 :
	       9'd0 ;

  // value method _MaintenanceReqIfc_mreq_local_
  assign _MaintenanceReqIfc_mreq_local_ =
	     EN__inputs_MaintenanceReqIfcPkt &&
	     _inputs_MaintenanceReqIfcPkt_value[0] ;

  // remaining internal signals
  assign x__read_mreqdata_mreq_byte_count__h463 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[9:1] :
	       9'd0 ;
  assign x__read_mreqdata_mreq_byte_en__h462 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[17:10] :
	       8'd0 ;
  assign x__read_mreqdata_mreq_data__h453 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[185:122] :
	       64'd0 ;
  assign x__read_mreqdata_mreq_dest_id__h458 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[110:79] :
	       32'd0 ;
  assign x__read_mreqdata_mreq_ftype__h456 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[118:115] :
	       4'd0 ;
  assign x__read_mreqdata_mreq_offset__h461 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[38:18] :
	       21'd0 ;
  assign x__read_mreqdata_mreq_source_id__h459 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[78:47] :
	       32'd0 ;
  assign x__read_mreqdata_mreq_tid__h460 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[46:39] :
	       8'd0 ;
  assign x__read_mreqdata_mreq_tt__h452 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[187:186] :
	       2'd0 ;
  assign x__read_mreqdata_mreq_ttype__h457 =
	     EN__inputs_MaintenanceReqIfcPkt ?
	       _inputs_MaintenanceReqIfcPkt_value[114:111] :
	       4'd0 ;
  always@(x__read_mreqdata_mreq_tt__h452 or
	  x__read_mreqdata_mreq_dest_id__h458)
  begin
    case (x__read_mreqdata_mreq_tt__h452)
      2'b01:
	  lv_MreqDestID__h502 =
	      { x__read_mreqdata_mreq_dest_id__h458[31:16], 16'h0 };
      2'b10: lv_MreqDestID__h502 = x__read_mreqdata_mreq_dest_id__h458;
      default: lv_MreqDestID__h502 =
		   { x__read_mreqdata_mreq_dest_id__h458[31:24], 24'h0 };
    endcase
  end
  always@(x__read_mreqdata_mreq_tt__h452 or
	  x__read_mreqdata_mreq_source_id__h459)
  begin
    case (x__read_mreqdata_mreq_tt__h452)
      2'b01:
	  lv_MreqSrcID__h590 =
	      { x__read_mreqdata_mreq_source_id__h459[31:16], 16'h0 };
      2'b10: lv_MreqSrcID__h590 = x__read_mreqdata_mreq_source_id__h459;
      default: lv_MreqSrcID__h590 =
		   { x__read_mreqdata_mreq_source_id__h459[31:24], 24'h0 };
    endcase
  end
endmodule  // mkRapidIO_MaintenanceReqIFC

