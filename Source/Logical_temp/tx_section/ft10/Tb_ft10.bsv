package Tb_ft10;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;

module mkTb_ft10(Empty);

	Ifc_InitiatorReqSignals initreq <- mkRapidIO_InitiatorReqIFC;
	Ifc_RapidIO_IOPktConcatenation pktcon <- mkRapidIO_IOPktConcatenation;
	Ifc_RapidIO_IOPkt_Generation gen <- mkRapidIO_IOPkt_Generation;

	Wire#(InitiatorReqIfcPkt) wr_ireq_pkt <- mkDWire(defaultValue);
	Wire#(TargetRespIfcPkt) wr_tgt_pkt <- mkDWire(defaultValue);
	Wire#(MaintenanceRespIfcPkt) wr_main_pkt <- mkDWire(defaultValue);

	Wire#(InitReqDataInput) wr_data_count <- mkDWire(defaultValue);

	Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 5)
		$finish (0);
	endrule

  rule r1_ft10(reg_ref_clk==0);
	initreq._InitReqIfc._ireq_sof_n(False);
	initreq._InitReqIfc._ireq_eof_n(False);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h0);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b1010);
	initreq._InitReqIfc._ireq_dest_id(32'h3245A00d);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(16'b1111000011111111);
	initreq._InitReqIfc._ireq_msg_len(4'b0110);
	initreq._InitReqIfc._ireq_msg_seg(4'b0010);
	initreq._InitReqIfc._ireq_mbox(6'b001000);
	initreq._InitReqIfc._ireq_letter(2'b10);	
  endrule

rule r1_ft101(reg_ref_clk==1);
	initreq._InitReqIfc._ireq_sof_n(False);
	initreq._InitReqIfc._ireq_eof_n(False);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b01);
	initreq._InitReqIfc._ireq_data(128'h0);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b1010);
	initreq._InitReqIfc._ireq_dest_id(32'h3245A00d);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(16'b1111000100000010);
	initreq._InitReqIfc._ireq_msg_len(4'b0110);
	initreq._InitReqIfc._ireq_msg_seg(4'b0010);
	initreq._InitReqIfc._ireq_mbox(6'b001000);
	initreq._InitReqIfc._ireq_letter(2'b10);	
  endrule

rule r1_ft102(reg_ref_clk==2);
	initreq._InitReqIfc._ireq_sof_n(False);
	initreq._InitReqIfc._ireq_eof_n(False);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h0);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b1010);
	initreq._InitReqIfc._ireq_dest_id(32'h3245A00d);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(16'b1111111111111111);
	initreq._InitReqIfc._ireq_msg_len(4'b0110);
	initreq._InitReqIfc._ireq_msg_seg(4'b0010);
	initreq._InitReqIfc._ireq_mbox(6'b001000);
	initreq._InitReqIfc._ireq_letter(2'b10);	
  endrule

  rule ready1;
	gen.pkgen_rdy_n (False);
  endrule

  rule ready2;
	pktcon._inputs_RxReady_From_IOGeneration(gen.outputs_RxRdy_From_Dest_());
  endrule

  rule ready3;
	initreq._inputs_IreqRDYIn_From_Concat(pktcon.outputs_RxRdy_From_Concat_());
  endrule


  rule r2_ft10;
	$display("Initiator request packet = %b",initreq.outputs_InitReqIfcPkt_ ());
	pktcon._inputs_InitReqIfcPkt(initreq.outputs_InitReqIfcPkt_ ());
  endrule

  rule r3_ft10;
	$display("FType 10 packet = %b",pktcon.outputs_Ftype10_MgDOORBELLClass_ ());
	gen._inputs_Ftype10MgDOORBELLClass(pktcon.outputs_Ftype10_MgDOORBELLClass_ ());
	gen._inputs_InitReqIfcPkt(pktcon.outputs_InitReqIfcPkt_ ());
  endrule

  rule r4_ft10;
	gen._inputs_InitReqDataCount(wr_data_count);
  endrule

  rule r5_ft10_genout;
	$display("Generation Module Outputs \n");
	$display("SOF = %b \n ",gen.pkgen_sof_n_ ());
	$display("EOF = %b \n ",gen.pkgen_eof_n_ ());
	$display("VLD = %b \n",gen.pkgen_vld_n_ ());
	$display("DSC = %b \n",gen.pkgen_dsc_n_ ());
	$display("TX_REM = %b \n",gen.pkgen_tx_rem_ ());
	$display("CRF = %b \n",gen.pkgen_crf_ ());
	$display("Data Packet = %h \n",gen.pkgen_data_());
	$display("RxRdy = %b",gen.outputs_RxRdy_From_Dest_());
  endrule

endmodule
endpackage
