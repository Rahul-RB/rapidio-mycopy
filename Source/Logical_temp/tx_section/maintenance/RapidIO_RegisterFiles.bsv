/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Capability Registers (CARs), Command and Status Registers (CSRs) Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module contains the Capability Registers (CARs) and Command and Status Registers (CSRs) 
--
--
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------

-- Configuration Space Byte Offset 		Registers
-- 	0x0					Device Identity CAR 
--	0x4					Device Information CAR
-- 	0x8					Assembly Identity CAR 
-- 	0xC					Assembly Information CAR
-- 	0x10					Processing Element Features CAR
--	0x14					Switch Port Information CAR
-- 	0x18					Source Operations CAR
-- 	0x1C 					Destination Operations CAR
-- 	0x20 - 0x48				-- Reserved -- 
-- 	0x4C					Processing Element Logical Layer Control (LLC) CSR
-- 	0x50					-- Reserved -- 
-- 	0x58					Local Configuration Space Base Address 0 (SBA0) CSR
-- 	0x5C					Local Configuration Space Base Address 1 CSR
--	0x60 - 0xFC				-- Reserved -- 
--
-------------------------------------------------------------------------------
*/

package RapidIO_RegisterFiles;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;

`include "RapidIO_RegisterFile_Offset.defines"

interface Ifc_RapidIO_RegisterFiles;
 // Input Methods
 method Action _inputs_RIORead (Bool value);
 method Action _inputs_RIOWrite (Bool value);
 
 method Action _inputs_Offset (Offset value);
 method Action _inputs_DataIn (Data value);

 // Output Methods 
 method Bit#(32) outputs_DataOut_ ();
 method Status outputs_Status_ ();
 
endinterface : Ifc_RapidIO_RegisterFiles

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)
module mkRapidIO_RegisterFiles (Ifc_RapidIO_RegisterFiles);

// Input Methods as Wires
Wire#(Bool) wr_Read <- mkDWire (False); // Read Operation 
Wire#(Bool) wr_Write <- mkDWire (False); // Write Operation

Wire#(Offset) wr_ConfigOffset <- mkDWire (0);
Wire#(Data) wr_DataIn <- mkDWire (0);

// Output Methods as Register 
Reg#(Bit#(32)) rg_DataOut <- mkReg (0);
Reg#(Status) rg_Status <- mkReg (4'b0111); // By Default, rg_Status contain Error value 

// Capability Registers (CARs)
Reg#(RegSize) rg_Dev_Identity_CAR <- mkReg (0); 
Reg#(RegSize) rg_Dev_Info_CAR <- mkReg (0);
Reg#(RegSize) rg_Assembly_Identity_CAR <- mkReg (0);
Reg#(RegSize) rg_Assembly_Info_CAR <- mkReg (0);
Reg#(RegSize) rg_Proc_Element_Features_CAR <- mkReg (0);
Reg#(RegSize) rg_Switch_Port_Info_CAR <- mkReg (0);
Reg#(RegSize) rg_Source_Operations_CAR <- mkReg (0);
Reg#(RegSize) rg_Dest_Operations_CAR <- mkReg (0);
Reg#(RegSize) rg_Switch_Route_CAR <- mkReg (0);


// Command and Status Registers (CSRs) 
Reg#(RegSize) rg_Proc_Element_LLC_CSR <- mkReg (0); // LLC - Logical Layer Control CSR 

Reg#(RegSize)  	rg_Local_Config_SBA0_CSR <- mkReg (0); // SBA - Space Base Address 0 CSR 
Reg#(RegSize) 	rg_Local_Config_SBA1_CSR <- mkReg (0); // SBA - Space Base Address 1 CSR 
Reg#(RegSize)   rg_Base_Dev_ID_CSR <- mkReg (0); 
Reg#(RegSize)	rg_Dev32_Base_Dev_ID_CSR<- mkReg (0); // Device 32 Base Device ID CSR
Reg#(RegSize)	rg_Host_Base_Dev_ID_CSR  <- mkReg (0); // Host Base Device ID Lock CSR
Reg#(RegSize)	rg_Comp_Tag_CSR <- mkReg (0) ; // Component Tag CSR
Reg#(RegSize)	rg_Stnd_Route_Dest_CSR <- mkReg (0); // Standard Route Cfg Destination ID  Select CSR
Reg#(RegSize)	rg_Stnd_Route_Port_CSR <- mkReg (0); // Standard Route Cfg Port Select CSR ;
Reg#(RegSize)	rg_Stnd_Route_Default_CSR <- mkReg (0);// Standard Route Default Port CSR;

// -- Rules -- 
/*
-- Read-Only Registers are assigned and Needs to change according to the 
-- specification of the RapidIO.
-- Only Read and No Write. 
*/
rule rl_AssigningReadOnlyValues;
	rg_Dev_Identity_CAR <= 32'hffffffff; // Device Identity and Device Vendor Identity fields (given by RapidIO) 
	rg_Dev_Info_CAR <= 32'hffffffff; // Device Revision Level (32 bits) 
	rg_Assembly_Identity_CAR <= 32'hffffffff; // Assembly Identity and Assembly Vendor Identity Feilds 
	rg_Assembly_Info_CAR <= 32'h0000ffff; // Assembly Revision Level 
	rg_Proc_Element_Features_CAR <= 32'h40000010; // Processing Element Features features 
	rg_Switch_Port_Info_CAR <= 32'hff030000; // Switch Port - Port Total and Port Number 
	rg_Source_Operations_CAR <= 32'h0000f7f8; // Source Operations 
	rg_Dest_Operations_CAR <= 32'h0000f7f8; // Destination Operations 
        rg_Switch_Route_CAR <=32'h00000000; // Switch Route Table Destination ID Limit CAR
	


	
	rg_Proc_Element_LLC_CSR <= {28'h0000000, 4'b0001}; // Processing Element Logical Layer Control CSR 
	rg_Local_Config_SBA0_CSR <= 32'h00000000; // SBA - Space Base Address 0 CSR 
	rg_Local_Config_SBA1_CSR <= 32'h00000000; // SBA - Space Base Address 1 CSR
	
	
	rg_Base_Dev_ID_CSR <= 32'h00ffffff; // Base Device ID CSR
	rg_Dev32_Base_Dev_ID_CSR<= 32'h00000000; // Device 32 Base Device ID CSR
	rg_Host_Base_Dev_ID_CSR <= 32'h0000ffff ; // Host Base Device ID Lock CSR
	rg_Comp_Tag_CSR<= 32'h00000000 ; // Component Tag CSR
	rg_Stnd_Route_Dest_CSR<= 32'h00000000 ; // Standard Route Cfg Destination ID  Select CSR
	rg_Stnd_Route_Port_CSR<= 32'h00000000; // Standard Route Cfg Port Select CSR ;
	rg_Stnd_Route_Default_CSR<=32'h00000000;// Standard Route Default Port CSR;
endrule

rule rl_ReadOperation; 
    if (wr_Read == True) begin
	case (wr_ConfigOffset) matches

// CARs

	'h0 : begin
		rg_DataOut <= rg_Dev_Identity_CAR; // input/output logical
		rg_Status <= 4'h0;
	      end
	'h4 : begin 
		rg_DataOut <= rg_Dev_Info_CAR; // input/output logical
		rg_Status <= 4'h0; 
	      end 
	'h8 : begin 
		rg_DataOut <= rg_Assembly_Identity_CAR; // input/output logical
		rg_Status <= 4'h0; 
 	      end 
	'hc : begin 
		rg_DataOut <= rg_Assembly_Info_CAR; // input/output logical
		rg_Status <= 4'h0; 
	      end 
	'h10 : begin 
		rg_DataOut <= rg_Proc_Element_Features_CAR; // input/output logical , common transport , LP serial physical layer , multicast 																											extensions
		rg_Status <= 4'h0; 
	       end 
	'h14 : begin 
		rg_DataOut <= rg_Switch_Port_Info_CAR; // input/output logical
		rg_Status <= 4'h0;
	       end 
	'h18 : begin 
		rg_DataOut <= rg_Source_Operations_CAR; // input/output logical , message passing logical , globally shared memory logical , data 																					streaming logical
		rg_Status <= 4'h0; 
	       end 
	'h1c : begin 
		rg_DataOut <= rg_Dest_Operations_CAR; // input/output logical , message passing logical , globally shared memory logical , data 																						streaming logical
		rg_Status <= 4'h0; 
 
    'h34 :begin	 
		rg_DataOut <= rg_Switch_Route_CAR ; // common transport
		rg_Status <= 4'h0; 
		 end
		 
 // CSRs
		 
	'h4C :begin	 
		rg_DataOut <= rg_Proc_Element_LLC_CSR ; // input/output logical
		rg_Status <= 4'h0; 
		 end 
		 
	'h58 :begin	 
		rg_DataOut <=rg_Local_Config_SBA0_CSR  ; // input/output logical
		rg_Status <= 4'h0; 
		 end
	'h5C :begin	 
		rg_DataOut <= rg_Local_Config_SBA1_CSR ; // input/output logical
		rg_Status <= 4'h0; 
		 end
	
	'h60 :begin	 
		rg_DataOut <= rg_Base_Dev_ID_CSR ; // common transport
		rg_Status <= 4'h0; 
		 end
		 
	'h64 :begin	 
		rg_DataOut <=rg_Dev32_Base_Dev_ID_CSR  ; // common transport
		rg_Status <= 4'h0; 
		 end
	
	'h68:begin	 
		rg_DataOut <= rg_Host_Base_Dev_ID_CSR ; // common transport
		rg_Status <= 4'h0; 
		 end
    
	'h6C :begin	 
		rg_DataOut <=rg_Comp_Tag_CSR  ; // common transport
		rg_Status <= 4'h0; 
		 end
		 
	'h70 :begin	 
		rg_DataOut <=rg_Stnd_Route_Dest_CSR  ; // common transport
		rg_Status <= 4'h0; 
		 end
		 
	'h74 :begin	 
		rg_DataOut <= rg_Stnd_Route_Port_CSR ; // common transport
		rg_Status <= 4'h0; 
		 end

	'h78 :begin	 
		rg_DataOut <= rg_Stnd_Route_Default_CSR ; // common transport
		rg_Status <= 4'h0; 
		 end	 
		 
		default : begin 
		rg_DataOut <= 0; 
		rg_Status <= 4'b0111; // Unrecoverable error 
		end 
	endcase
    end 
endrule 

// Methods Definition 
 // Input Methods
 method Action _inputs_RIORead (Bool value);
	wr_Read <= value; 
 endmethod 
 method Action _inputs_RIOWrite (Bool value);
	wr_Write <= value; 
 endmethod 
 
 method Action _inputs_Offset (Offset value);
	wr_ConfigOffset <= value; 	
 endmethod 
 method Action _inputs_DataIn (Data value);
	wr_DataIn <= value; 
 endmethod 

 // Output Methods 
 method Bit#(32) outputs_DataOut_ ();
	return rg_DataOut; 
 endmethod
 method Status outputs_Status_ ();
	return rg_Status; 
 endmethod 

endmodule : mkRapidIO_RegisterFiles

endpackage : RapidIO_RegisterFiles
