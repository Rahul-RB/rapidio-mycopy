package fifo;

import FIFO ::*;
import FIFOF ::*;
import SpecialFIFOs ::*;
import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_TxFTypeFunctionsDev8 ::*;
import RapidIO_TxFTypeFunctionsDev16 ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;


interface Ifc_fifo;
 
  method Action _input_transmit_pkt (Transmit_Pkt pkt);
  method Action rdy_n (Bool value);

 method Bool sof_n_ (); 
 method Bool eof_n_ (); 
 method Bool vld_n_ (); 
 method Bool dsc_n_ (); 
 method DataPkt data_ (); 
 method Bit#(4) tx_rem_ ();
 method Bool crf_ (); 

 method Bool outputs_RxRdy_From_Dest_();
endinterface

(* synthesize *)

module mkfifo(Ifc_fifo);

//Ifc_RapidIO_IOPkt_Generation pkgen <- mkRapidIO_IOPkt_Generation;

Wire#(Bool) wr_RxReady_In <- mkDWire (False);

Wire#(Transmit_Pkt) wr_Transmit_Pkt <- mkDWire(defaultValue);

Wire#(Transmit_Pkt) wr_PktGenTranmitIfcFirst <- mkDWire (defaultValue);

FIFOF#(Transmit_Pkt) ff_TransmitPktIfcFIFO <- mkSizedBypassFIFOF (16);


Reg#(Bit#(4)) rg_fifo <- mkReg(1);
Wire#(Bit#(4)) wr_fifo <- mkDWire(1);


Reg#(Bool) _SOF_n <- mkReg (False);
Reg#(Bool) _EOF_n <- mkReg (False);
Reg#(Bool) _DSC_n <- mkReg (False);
Reg#(Bool) _VLD_n <- mkReg (False); 
Reg#(Bit#(4)) rg_TxRem <- mkReg (0);

Reg#(InitiatorReqIfcPkt) rg_InitReqInput1Delay <- mkReg (defaultValue);
Reg#(InitiatorReqIfcPkt) rg_InitReqInput2Delay <- mkReg (defaultValue);

Reg#(TargetRespIfcPkt) rg_TgtRespInput1Delay <- mkReg (defaultValue); 
Reg#(TargetRespIfcPkt) rg_TgtRespInput2Delay <- mkReg (defaultValue);

Wire#(InitiatorReqIfcPkt) wr_InitReqInput <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_TgtRespInput <- mkDWire (defaultValue);

rule rl_InitReqInterfaceInput2Delay;

    rg_InitReqInput1Delay <= wr_InitReqInput;	// Initiator Request delayed 1 Clock cycle
    rg_TgtRespInput1Delay <= wr_TgtRespInput; 	// Target Response delayed 1 clock cycle
    rg_InitReqInput2Delay <= rg_InitReqInput1Delay;	// Initiator Request delayed 2 clock cycle
    rg_TgtRespInput2Delay <= rg_TgtRespInput1Delay;	// Target Response delayed 2 clock cycle
  
endrule


rule valid;
	if(wr_Transmit_Pkt.vld == True)
      begin
		ff_TransmitPktIfcFIFO.enq(wr_Transmit_Pkt);
rg_fifo <= rg_fifo +1;
	  end
endrule

rule regwire;
 wr_fifo <= rg_fifo;
endrule

/*
-- Following rule is used to clear the FIFO entries. 
-- Discontinue signal is used to clear the FIFO entries. 
*/

rule rl_CalculateDSC ((rg_InitReqInput2Delay.ireqcntrl.ireq_dsc == True) || (rg_TgtRespInput2Delay.trespcntrl.tresp_dsc == True));
    if ((rg_InitReqInput2Delay.ireqcntrl.ireq_dsc == True)) begin
	_DSC_n <= True;
	ff_TransmitPktIfcFIFO.clear();
    end 
    else if ((rg_TgtRespInput2Delay.trespcntrl.tresp_dsc == True) ) begin 
	_DSC_n <= False;
	ff_TransmitPktIfcFIFO.clear();
    end 
endrule



/*
-- Following Rules are used to store the Transmit Packets and Control signals ina FIFO.
-- Depend on the Valid and Ready Signals, the Enqueue and Dequeue are controlled. 
-- When Valid is True, the control and Data Packets are enqueued in the FIFO. 
-- When Valid and Ready are True, the FIFO Dequeue is performed. 
*/
rule rl_FIFOF_Dequeue ((ff_TransmitPktIfcFIFO.first.vld == True) && (wr_RxReady_In == False) && (wr_fifo >=4'b0100)); // Ready considered as Active low 
	ff_TransmitPktIfcFIFO.deq();
endrule 

/*
-- By default, Output reads the top of the FIFO Data. 
*/
rule rl_FIFOF_First (wr_fifo >= 4'b0100);
	wr_PktGenTranmitIfcFirst <= ff_TransmitPktIfcFIFO.first();
endrule 

rule disp;
//    $display ("\n\tThe IOGen Transmit Packet FIFO Output == %b",wr_PktGenTranmitIfcFirst);
$display ("\n Count Reg value == %b", rg_fifo);
endrule




method Action _input_transmit_pkt (Transmit_Pkt pkt);
	wr_Transmit_Pkt <= pkt;
endmethod

 method Action rdy_n (Bool value); 
	wr_RxReady_In <= value;
 endmethod


// Output Ports as Methods
 method Bool sof_n_ ();
	return !(wr_PktGenTranmitIfcFirst.sof); 
 endmethod

 method Bool eof_n_ ();
	return !(wr_PktGenTranmitIfcFirst.eof);
 endmethod	

 method Bool vld_n_ ();
	return !(wr_PktGenTranmitIfcFirst.vld);
 endmethod

 method Bool dsc_n_ ();
	return (!_DSC_n);
 endmethod

 method DataPkt data_();
	return wr_PktGenTranmitIfcFirst.data; 
 endmethod

 method Bit#(4) tx_rem_ ();
	return wr_PktGenTranmitIfcFirst.txrem; 
 endmethod

 method Bool crf_ ();
	return False;
 endmethod

 method Bool outputs_RxRdy_From_Dest_();
	return wr_RxReady_In; 
 endmethod 

endmodule
endpackage
