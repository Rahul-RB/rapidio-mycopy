package Tb_PktTransportParse;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse ::*;



module mkTb_PktTransportParse(Empty);

Ifc_RapidIO_PktTransportParse pktTransportParse <- mkRapidIO_PktTransportParse;

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 4)
		$finish (0);
	endrule


/////////////////////// ftype 6
rule r1(reg_ref_clk == 0);
	pktTransportParse._PktParseRx_SOF_n(False);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h004a320000f2f0ff0000000000000000);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule display;
	//$display("Type 2 request class == %h",pktTransportParse.outputs_RxFtype2ReqClass_ ());
	//$display("Type 5 write class == %h",pktTransportParse.outputs_RxFtype5WriteClass_ ());
	//$display("Type 6 streaming write class == %h",pktTransportParse.outputs_RxFtype6StreamClass_ ());
	//$display("Type 6 streaming write class data == %h",pktTransportParse.outputs_RxFtype6StreamData_ ());
	//$display("Type 8 maintenance class == %h",pktTransportParse.outputs_RxFtype8MainReqClass_());
	$display("Type 10 doorbell class == %h",pktTransportParse.outputs_RxFtype10DoorBellClass_ ());
	/*$display("Type 11 message passing class header == %h",pktTransportParse.outputs_RxFtype11MsgHeader_ ());
	$display("Type 11 message passing class data == %h",pktTransportParse.outputs_RxFtype11Data_ ());
	$display("Type 13 response class == %h",pktTransportParse.outputs_RxFtype13ResponseClass_ ()); 
	$display("",PktTransportParse.outputs_RxFtype13ResponseData_ ()); */
	$display("Received packets == %h",pktTransportParse.outputs_ReceivedPkts_ ());
	$display("Received TT == %b",pktTransportParse.outputs_TTReceived_ ());
	$display("Received destination id == %b",pktTransportParse.outputs_RxDestId_ ()); 
	$display("Received source id == %b",pktTransportParse.outputs_RxSourceId_ ()); 
	$display("Received prio field == %b",pktTransportParse.outputs_RxPrioField_ ()); 
	$display("Max packet count == %b",pktTransportParse.outputs_MaxPktCount_ ()); 
endrule

endmodule

endpackage
