package Tb_PktTransportParse;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse ::*;



module mkTb_PktTransportParse(Empty);

Ifc_RapidIO_PktTransportParse pktTransportParse <- mkRapidIO_PktTransportParse;

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 4)
		$finish (0);
	endrule


/////////////////////// ftype 8
rule r1(reg_ref_clk == 0);
	pktTransportParse._PktParseRx_SOF_n(False);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h004882002ff200000000111111111111);
	pktTransportParse._PktParseRx_rem(4'b0011);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r12(reg_ref_clk == 1);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(False);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h11111111111111111111110000000000);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule display;
	$display("Type 8 maintenance class == %h",pktTransportParse.outputs_RxFtype8MainReqClass_());
	$display("Type 8 maintenance class data == %h",pktTransportParse.outputs_RxFtype8MaintainData_());
	$display("Received packets == %h",pktTransportParse.outputs_ReceivedPkts_ ());
	$display("Received TT == %b",pktTransportParse.outputs_TTReceived_ ());
	$display("Received destination id == %b",pktTransportParse.outputs_RxDestId_ ()); 
	$display("Received source id == %b",pktTransportParse.outputs_RxSourceId_ ()); 
	$display("Received prio field == %b",pktTransportParse.outputs_RxPrioField_ ()); 
	$display("Max packet count == %b",pktTransportParse.outputs_MaxPktCount_ ()); 
endrule

endmodule

endpackage
