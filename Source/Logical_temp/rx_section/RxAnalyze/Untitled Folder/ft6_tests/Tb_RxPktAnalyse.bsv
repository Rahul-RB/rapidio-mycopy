package Tb_RxPktAnalyse;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse ::*;
import RapidIO_RxPktFTypeAnalyse ::*;


module mkTb_RxPktAnalyse(Empty);

Ifc_RapidIO_PktTransportParse pktTransportParse <- mkRapidIO_PktTransportParse;
Ifc_RapidIO_RxPktFTypeAnalyse rxpktanalyse <- mkRapidIO_RxPktFTypeAnalyse;

Reg#(Bit#(5)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 20)
		$finish (0);
	endrule



///////////////////////// ftype 6

rule r1(reg_ref_clk == 0);
	pktTransportParse._PktParseRx_SOF_n(False);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h005683990000000000000008ffffffff);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r12_1(reg_ref_clk == 1);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h11111111111111111111111111111111);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r13_2(reg_ref_clk == 2);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h22222222222222222222222222222222);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule


rule r14(reg_ref_clk == 3);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r1412(reg_ref_clk == 4);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h44444444444444444444444444444444);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r5(reg_ref_clk == 5);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r64(reg_ref_clk == 6);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r17(reg_ref_clk == 7);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r148(reg_ref_clk == 8);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r9(reg_ref_clk == 9);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r11(reg_ref_clk == 10);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r1511(reg_ref_clk == 11);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r12(reg_ref_clk == 12);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r13(reg_ref_clk == 13);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule
rule r144(reg_ref_clk == 14);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(True);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333333333333);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule

rule r1445(reg_ref_clk == 15);
	pktTransportParse._PktParseRx_SOF_n(True);
	pktTransportParse._PktParseRx_EOF_n(False);
	pktTransportParse._PktParseRx_VLD_n(False);
	pktTransportParse._PktParseRx_data(128'h33333333333333333333333300000000);
	pktTransportParse._PktParseRx_rem(4'b0000);
	pktTransportParse._PktParseRx_crf(False);
	pktTransportParse._inputs_TxReadyIn_From_Analyze(True);
endrule



rule rl_rxanalysemodule;
	rxpktanalyse._inputs_ReceivedPkts(pktTransportParse.outputs_ReceivedPkts_ ());
	rxpktanalyse._inputs_RxFtype6StreamClass(pktTransportParse.outputs_RxFtype6StreamClass_ ());
	rxpktanalyse._inputs_RxFtype6StreamData(pktTransportParse.outputs_RxFtype6StreamData_ ());
	rxpktanalyse._inputs_TTReceived(pktTransportParse.outputs_TTReceived_ ());
	rxpktanalyse._inputs_RxDestId(pktTransportParse.outputs_RxDestId_ ());
	rxpktanalyse._inputs_RxSourceId(pktTransportParse.outputs_RxSourceId_ ());
	rxpktanalyse._inputs_RxPrioField(pktTransportParse.outputs_RxPrioField_ ());
	rxpktanalyse._inputs_MaxPktCount(pktTransportParse.outputs_MaxPktCount_ ());
	rxpktanalyse._inputs_TxReady_From_IResp(True);
endrule

rule display;
	
$display("\n************************* packet transport parse outputs *****************************");
$display("received packets == %h",pktTransportParse.outputs_ReceivedPkts_ ());
$display("ftype6 stream class == %h",pktTransportParse.outputs_RxFtype6StreamClass_ ());
$display("ftype6 data == %h",pktTransportParse.outputs_RxFtype6StreamData_ ());

$display("\n$$$$$$$$$$$$$$$$$$ receiver analyze outputs $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
$display("Target request output packets == %b \n",rxpktanalyse.outputs_TargetReqIfcPkt_ ());

endrule



endmodule

endpackage
